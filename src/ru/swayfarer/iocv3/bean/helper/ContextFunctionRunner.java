package ru.swayfarer.iocv3.bean.helper;

import lombok.var;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.context.Context;
import ru.swayfarer.iocv3.helper.ContextHelper;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

@Getter
@Setter
@Accessors(chain = true)
public class ContextFunctionRunner<Return_Type> {

    public ContextHelper contextHelper;
    public IFunction1<Context, Return_Type> fun;

    public ContextFunctionRunner(ContextHelper contextHelper, IFunction1<Context, Return_Type> fun)
    {
        this.fun = fun;
        this.contextHelper = contextHelper;
    }

    public Return_Type in(Class<?> cl)
    {
        var contextHelper = getContextHelper();
        var contextFinder = contextHelper.getContextFinder();

        var contextUtils = contextFinder.findContext(cl);

        if (contextUtils == null)
        {
            contextUtils = contextHelper.getContextRegistry().getDefaultContext();
        }

        return in(contextUtils);
    }

    public Return_Type in(@NonNull Object instance, Method method)
    {
        return in(instance.getClass(), method, instance);
    }

    public Return_Type in(Class<?> cl, Method method)
    {
        return in(cl, method, null);
    }

    public Return_Type in(Class<?> cl, Method method, Object instance)
    {
        var contextUtils = getContextHelper().getContextFinder().findContext(cl, method, instance);
        return in(contextUtils);
    }

    public Return_Type in(@NonNull Object instance, Field field)
    {
        return in(instance.getClass(), field, instance);
    }

    public Return_Type in(Class<?> cl, Field field)
    {
        return in(cl, field, null);
    }

    public Return_Type in(Class<?> cl, Field field, Object instance)
    {
        var contextUtils = getContextHelper().getContextFinder().findContext(cl, field, instance);
        return in(contextUtils);
    }

    public Return_Type in(String contextName)
    {
        return in(getContextHelper().getContextRegistry().findOrCreateContext(contextName));
    }

    public Return_Type in(ContextUtils contextUtils)
    {
        return in(contextUtils.getContext());
    }

    public Return_Type in(Context context)
    {
        return fun.apply(context);
    }
}
