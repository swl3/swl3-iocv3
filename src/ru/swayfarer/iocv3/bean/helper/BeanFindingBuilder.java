package ru.swayfarer.iocv3.bean.helper;

import lombok.var;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.BeanInfo;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.helper.ContextHelper;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.reflection.generic.GenericObject;

@Getter
@Setter
@Accessors(chain = true)
public class BeanFindingBuilder {

    public ContextHelper contextHelper;

    public boolean required;
    public BeanInfo targetBeanInfo = new BeanInfo();

    public BeanFindingBuilder(ContextHelper contextHelper)
    {
        this.contextHelper = contextHelper;
    }

    public BeanFindingBuilder names(String... names)
    {
        targetBeanInfo.getNames().addAll(names);
        return this;
    }

    public BeanFindingBuilder type(Class<?> javaType)
    {
        targetBeanInfo.getAccociatedType().setClassType(javaType);
        return this;
    }

    public BeanFindingBuilder canFindByType(boolean canFind)
    {
        targetBeanInfo.setCanFindByType(canFind);
        return this;
    }

    public BeanFindingBuilder tags(String... tags)
    {
        for (var tag : tags)
            targetBeanInfo.data(tag);

        return this;
    }

    public BeanFindingBuilder tags(ExtendedList<String> tags)
    {
        for (var tag : tags)
            targetBeanInfo.data(tag);

        return this;
    }

    public BeanFindingBuilder generics(GenericObject... generics)
    {
        generics(CollectionsSWL.list(generics));
        return this;
    }

    public BeanFindingBuilder generics(ExtendedList<GenericObject> generics)
    {
        targetBeanInfo.getAccociatedType().setGenerics(generics);
        return this;
    }

    public ContextFunctionRunner<ContextBean> get()
    {
        return new ContextFunctionRunner<>(contextHelper, (context) -> {
            var event = new BeanFindingEvent(context);
            event.setTargetBeanInfo(getTargetBeanInfo());
            event.setBeanRequired(isRequired());
            return getContextHelper().getBeanFinder().findBean(event);
        });
    }
}
