package ru.swayfarer.iocv3.bean;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.reflection.generic.GenericObject;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

import java.util.concurrent.atomic.AtomicInteger;

@Data
@Accessors(chain = true)
public class BeanType {
    public Class<?> classType;
    public ExtendedList<GenericObject> generics = CollectionsSWL.list();
    
    public static BeanType objectType = new BeanType()
        .setClassType(Object.class)
    ;
    
    public static BeanType of(@NonNull Class<?> classOfType)
    {
        return new BeanType()
                .setClassType(classOfType)
        ;
    }
    
    public BeanType copy()
    {
        var ret = new BeanType();
        ret.setClassType(classType);
        ret.setGenerics(generics.copy());
        return ret;
    }

    public void toFancyString(DynamicString str, int indentValue)
    {
        var indent = new AtomicInteger(indentValue);
        IFunction0NoR newline = () -> {
            str.newLine().indent(indent.get());
        };

        str.append("java-associated: {");
        indent.incrementAndGet();
        newline.apply();

        str.append("class: ");
        str.append(String.valueOf(classType));
        newline.apply();

        str.append("generics: ");
        str.append(generics.map(GenericObject::toGenericString));

        indent.decrementAndGet();
        newline.apply();
        str.append("}");
        newline.apply();
    }
}
