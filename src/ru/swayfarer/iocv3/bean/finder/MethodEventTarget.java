package ru.swayfarer.iocv3.bean.finder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.ExtendedOptional;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;

@Data
@NoArgsConstructor @AllArgsConstructor
@Accessors(chain = true)
public class MethodEventTarget {
    
    public Object instanceWithMethod;
    
    public boolean constructor;
    
    @NonNull
    public Class<?> classWithMethod;
    
    @NonNull
    public String methodName;
    
    @NonNull
    public Class<?> returnType;
    
    @NonNull
    public Type genericReturnType;
    
    @NonNull
    public ExtendedList<Annotation> annotations = new ExtendedList<>();

    @NonNull
    public ExtendedList<Parameter> parameters = new ExtendedList<>();

    public static MethodEventTarget readMethod(Constructor<?> method, Object instanceWithMethod)
    {
        return readMethod(method)
                .setInstanceWithMethod(instanceWithMethod)
                .setClassWithMethod(instanceWithMethod.getClass())
                .setReturnType(method.getDeclaringClass())
        ;
    }

    public static MethodEventTarget readMethod(Constructor<?> method)
    {
        return new MethodEventTarget()
                .setMethodName(method.getName())
                .setGenericReturnType(method.getDeclaringClass().getGenericSuperclass())
                .setAnnotations(CollectionsSWL.list(method.getAnnotations()))
                .setParameters(CollectionsSWL.list(method.getParameters()))
                .setConstructor(false)
        ;
    }

    public static MethodEventTarget readMethod(Method method, Object instanceWithMethod)
    {
        return readMethod(method)
                .setInstanceWithMethod(instanceWithMethod)
                .setClassWithMethod(ExtendedOptional.of(instanceWithMethod).map(Object::getClass).orNull())
        ;
    }

    public static MethodEventTarget readMethod(Method method)
    {
        return new MethodEventTarget()
            .setMethodName(method.getName())
            .setGenericReturnType(method.getDeclaringClass().getGenericSuperclass())
            .setReturnType(method.getReturnType())
            .setAnnotations(CollectionsSWL.list(method.getAnnotations()))
            .setParameters(CollectionsSWL.list(method.getParameters()))
            .setConstructor(false)
        ;
    }
}
