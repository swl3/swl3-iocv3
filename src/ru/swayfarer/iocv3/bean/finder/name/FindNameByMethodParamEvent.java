package ru.swayfarer.iocv3.bean.finder.name;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.bean.finder.ParameterEventTarget;

public class FindNameByMethodParamEvent extends FindBeanNameEvent<ParameterEventTarget>{

    public FindNameByMethodParamEvent(@NonNull ParameterEventTarget targetType, @NonNull BeanFindingEvent beanFindingEvent)
    {
        super(targetType, beanFindingEvent);
    }
}
