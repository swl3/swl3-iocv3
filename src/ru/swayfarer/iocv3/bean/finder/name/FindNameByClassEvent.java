package ru.swayfarer.iocv3.bean.finder.name;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.bean.finder.ClassEventTarget;

public class FindNameByClassEvent extends FindBeanNameEvent<ClassEventTarget>{

    public FindNameByClassEvent(@NonNull ClassEventTarget targetType, BeanFindingEvent beanFindingEvent)
    {
        super(targetType, beanFindingEvent);
    }
}
