package ru.swayfarer.iocv3.bean.finder.name;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.bean.finder.MethodEventTarget;

public class FindNameByMethodEvent extends FindBeanNameEvent<MethodEventTarget>{

    public FindNameByMethodEvent(@NonNull MethodEventTarget targetType, BeanFindingEvent beanFindingEvent)
    {
        super(targetType, beanFindingEvent);
    }
}
