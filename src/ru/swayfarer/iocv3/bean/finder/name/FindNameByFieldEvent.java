package ru.swayfarer.iocv3.bean.finder.name;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.bean.finder.FieldEventTarget;

public class FindNameByFieldEvent extends FindBeanNameEvent<FieldEventTarget>{

    public FindNameByFieldEvent(@NonNull FieldEventTarget targetType, @NonNull BeanFindingEvent beanFindingEvent)
    {
        super(targetType, beanFindingEvent);
    }
}
