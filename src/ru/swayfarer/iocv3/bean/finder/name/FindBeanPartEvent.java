package ru.swayfarer.iocv3.bean.finder.name;

import lombok.var;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.swl3.observable.event.AbstractCancelableEvent;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class FindBeanPartEvent<Result_Type, Target_Type> extends AbstractCancelableEvent{
    public Result_Type result;
    
    @NonNull
    public Target_Type targetType;
    
    public BeanFindingEvent beanFindingEvent;
    
    public FindBeanPartEvent(Target_Type targetType, BeanFindingEvent beanFindingEvent)
    {
        this.targetType = targetType;
        this.beanFindingEvent = beanFindingEvent;
    }
}
