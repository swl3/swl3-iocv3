package ru.swayfarer.iocv3.bean.finder.name.rule;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import java.lang.annotation.Annotation;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.BeanName;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

@Getter
@Setter
@Accessors(chain = true)
public class AnnotationsBeanNameRule {
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;

    public AnnotationsBeanNameRule(@NonNull IFunction0<ReflectionsUtils> reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
    }

    public ExtendedList<String> apply(@NonNull Annotation[] annotations)
    {
        var reflectionsUtils = getReflectionsUtils();
        
        var foundName = reflectionsUtils.annotations().findProperty()
            .name("value")
            .type(String.class)
            .marker(BeanName.class)
            .in(annotations)
            .get()
        ;
        
        return StringUtils.isEmpty(foundName) ? null : CollectionsSWL.list(foundName);
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
