package ru.swayfarer.iocv3.bean.finder.name.rule;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.finder.name.FindNameByFieldEvent;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

@Data
@Accessors(chain = true)
public class FieldBeanNameRule implements IFunction1<FindNameByFieldEvent, ExtendedList<String>> {

    @NonNull
    public IFunction0<AnnotationsBeanNameRule> annotationsBeanNameRule;
    
    @Override
    public ExtendedList<String> applyUnsafe(FindNameByFieldEvent event)
    {
        var annotationsBeanNameRule = getAnnotationsBeanNameRule();
        var field = event.getTargetType().getField();
        
        var ret = annotationsBeanNameRule.apply(field.getAnnotations());
        
        if (ret != null)
            return ret;
        
        ret = CollectionsSWL.list(field.getName());
        
        return ret;
    }
    
    public AnnotationsBeanNameRule getAnnotationsBeanNameRule()
    {
        return annotationsBeanNameRule.apply();
    }
}
