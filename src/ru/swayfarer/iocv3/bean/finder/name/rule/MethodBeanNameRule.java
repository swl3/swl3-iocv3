package ru.swayfarer.iocv3.bean.finder.name.rule;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import java.lang.annotation.Annotation;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.finder.name.FindNameByMethodEvent;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

@Getter
@Setter
@Accessors(chain = true)
public class MethodBeanNameRule implements IFunction1<FindNameByMethodEvent, ExtendedList<String>> {
    
    @NonNull
    public ExtendedList<String> prefixes = CollectionsSWL.list("is", "get");
    
    @NonNull
    public IFunction0<AnnotationsBeanNameRule> annotationsBeanNameRule;

    public MethodBeanNameRule(@NonNull IFunction0<AnnotationsBeanNameRule> annotationsBeanNameRule)
    {
        this.annotationsBeanNameRule = annotationsBeanNameRule;
    }

    @Override
    public ExtendedList<String> applyUnsafe(FindNameByMethodEvent event)
    {
        var annotationsBeanNameRule = getAnnotationsBeanNameRule();
        var annotations = event.getTargetType().getAnnotations().toArray(Annotation.class);
        var ret = annotationsBeanNameRule.apply(annotations);
        
        if (ret != null)
            return ret;
        
        var s = event.getTargetType().getMethodName();
        
        for (var prefix : prefixes)
        {
            var prefixLength = prefix.length();
            
            if (s.startsWith(prefix) && s.length() > prefixLength)
            {
                char ch = s.charAt(prefixLength);
                if (Character.isUpperCase(ch))
                {
                    s = s.substring(prefixLength);
                    break;
                }
            }
        }
        
        ret = CollectionsSWL.list(s);
        
        return ret;
    }
    
    public AnnotationsBeanNameRule getAnnotationsBeanNameRule()
    {
        return annotationsBeanNameRule.apply();
    }
}
