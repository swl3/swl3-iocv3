package ru.swayfarer.iocv3.bean.finder.name.rule;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.finder.name.FindNameByMethodParamEvent;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

import java.lang.reflect.Parameter;

@Getter
@Setter
@Accessors(chain = true)
public class ParameterBeanNameRule implements IFunction1<FindNameByMethodParamEvent, ExtendedList<String>> {

    @NonNull
    public AsmUtilsV2 asmUtils;

    @NonNull
    public IFunction0<AnnotationsBeanNameRule> annotationsBeanNameRule;
    
    @NonNull
    public ILogger logger = LogFactory.getLogger();

    public ParameterBeanNameRule(AsmUtilsV2 asmUtils, @NonNull IFunction0<AnnotationsBeanNameRule> annotationsBeanNameRule)
    {
        this.annotationsBeanNameRule = annotationsBeanNameRule;
        this.asmUtils = asmUtils;
    }

    @Override
    public ExtendedList<String> applyUnsafe(FindNameByMethodParamEvent event)
    {
        var annotationsBeanNameRule = getAnnotationsBeanNameRule();
        var param = event.getTargetType().getParameter();
        
        var ret = annotationsBeanNameRule.apply(param.getAnnotations());
        
        if (ret != null)
            return ret;
        
        if (param.isNamePresent())
        {
            ret = CollectionsSWL.list(param.getName());
        }
        else
        {
            var fieldEvent = event.getTargetType();
            var methodEvent = event.getTargetType().getMethod();

            ExtendedList<String> asmParameterNames = null;

            var classNode = asmUtils.classes().findByClass(methodEvent.getClassWithMethod()).orNull();

            if (classNode != null)
            {
                var mf = asmUtils.methods().filters();

                var buf = new DynamicString();

                methodEvent.getParameters().exStream()
                        .map(Parameter::getType)
                        .map(Type::getDescriptor)
                        .each(buf::append)
                ;

                var startDesc = buf.toString();

                var methodNode = asmUtils.methods().stream(classNode)
                        .filter(mf.name().equal(methodEvent.getMethodName()))
                        .filter((mn) -> mn.desc.startsWith(startDesc))
                        .findAny()
                        .orNull()
                ;

                if (methodNode != null && methodNode.parameters != null)
                {
                    asmParameterNames = new ExtendedList<>();
                    for (var paramNode : methodNode.parameters)
                    {
                        asmParameterNames.add(paramNode.name);
                    }
                }
            }

            var logPrefix = StringUtils.concatWithSpaces("Method", methodEvent.getClassWithMethod().getName() + "::" + methodEvent.getMethodName(), "was compiled without parameters info!");

            if (asmParameterNames != null)
            {
                var name = asmParameterNames.get(fieldEvent.getIndex());
                logger.warn(logPrefix, "Parameter names was taken from class bytecode...");
                ret = CollectionsSWL.list(name);
            }
            else
            {
                ExceptionsUtils.throwException(new IllegalStateException(logPrefix + ". Can't resolve method parameter name from"));
            }
        }
        
        return ret;
    }
    
    public AnnotationsBeanNameRule getAnnotationsBeanNameRule()
    {
        return annotationsBeanNameRule.apply();
    }
}
