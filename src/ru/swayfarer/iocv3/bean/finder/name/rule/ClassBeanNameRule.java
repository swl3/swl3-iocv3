package ru.swayfarer.iocv3.bean.finder.name.rule;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.finder.name.FindNameByClassEvent;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.string.StringUtils;

@Getter
@Setter
@Accessors(chain = true)
public class ClassBeanNameRule implements IFunction1<FindNameByClassEvent, ExtendedList<String>> {

    @NonNull
    public IFunction0<AnnotationsBeanNameRule> annotationsBeanNameRule;

    public ClassBeanNameRule(@NonNull IFunction0<AnnotationsBeanNameRule> annotationsBeanNameRule)
    {
        this.annotationsBeanNameRule = annotationsBeanNameRule;
    }

    @Override
    public ExtendedList<String> applyUnsafe(FindNameByClassEvent event)
    {
        var type = event.getTargetType().getType();
        var annotationsRule = getAnnotationsBeanNameRule();
        var ret = annotationsRule.apply(type.getAnnotations());

        if (ret != null)
            return ret;
        ret = CollectionsSWL.list(StringUtils.firstCharToLowerCase(type.getSimpleName()));

        if (ret != null)
            return ret;
        
        return ret;
    }
    
    public AnnotationsBeanNameRule getAnnotationsBeanNameRule()
    {
        return annotationsBeanNameRule.apply();
    }
}
