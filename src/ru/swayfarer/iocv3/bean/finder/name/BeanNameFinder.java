package ru.swayfarer.iocv3.bean.finder.name;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.finder.ClassEventTarget;
import ru.swayfarer.iocv3.bean.finder.MethodEventTarget;
import ru.swayfarer.iocv3.bean.finder.ParameterEventTarget;
import ru.swayfarer.iocv3.bean.finder.name.rule.AnnotationsBeanNameRule;
import ru.swayfarer.iocv3.bean.finder.name.rule.ClassBeanNameRule;
import ru.swayfarer.iocv3.bean.finder.name.rule.FieldBeanNameRule;
import ru.swayfarer.iocv3.bean.finder.name.rule.MethodBeanNameRule;
import ru.swayfarer.iocv3.bean.finder.name.rule.ParameterBeanNameRule;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Getter
@Setter
@Accessors(chain = true)
public class BeanNameFinder {
    
    @NonNull
    public FunctionsChain1<FindNameByClassEvent, ExtendedList<String>> findByClassFuns = new FunctionsChain1<>();
    
    @NonNull
    public FunctionsChain1<FindNameByFieldEvent, ExtendedList<String>> findByFieldFuns = new FunctionsChain1<>();
    
    @NonNull
    public FunctionsChain1<FindNameByMethodEvent, ExtendedList<String>> findByMethodFuns = new FunctionsChain1<>();
    
    @NonNull
    public FunctionsChain1<FindNameByMethodParamEvent, ExtendedList<String>> findByParamFuns = new FunctionsChain1<>();
    
    @NonNull 
    public IObservable<FindNameByMethodParamEvent> eventFindByParam = Observables.createObservable();
    
    public ExtendedList<String> findNames(Class<?> classToScan)
    {
        var classTarget = new ClassEventTarget(classToScan);
        return findNames(new FindNameByClassEvent(classTarget, null));
    }
    
    public ExtendedList<String> findNames(@NonNull FindNameByClassEvent event)
    {
        return findByClassFuns.apply(event);
    }
    
    public ExtendedList<String> findNames(@NonNull FindNameByMethodParamEvent event)
    {
        return findByParamFuns.apply(event);
    }
    
    public ExtendedList<String> findNames(@NonNull FindNameByFieldEvent event) 
    {
        return findByFieldFuns.apply(event);
    }
    
    public ExtendedList<String> findNames(@NonNull FindNameByMethodEvent event) 
    {
        return findByMethodFuns.apply(event);
    }
    
    public ExtendedList<String> findNames(Class<?> classOfMethod, Method method, Object instance)
    {
        var methodEvent = new FindNameByMethodEvent(
                new MethodEventTarget(
                        instance, 
                        false, 
                        classOfMethod, 
                        method.getName(), 
                        method.getReturnType(), 
                        method.getGenericReturnType(), 
                        CollectionsSWL.list(method.getAnnotations()),
                        CollectionsSWL.list(method.getParameters())
                ),
                null
        );
        
        return findNames(methodEvent);
    }
    
    public ExtendedList<String> findNames(Class<?> classOfMethod, Method method, int paramIndex, Object instance)
    {
        var param = method.getParameters()[paramIndex];

        var paramTarget = new ParameterEventTarget(
                MethodEventTarget.readMethod(method, instance).setClassWithMethod(classOfMethod),
                param,
                paramIndex
        );
        
        var event = new FindNameByMethodParamEvent(paramTarget, null);

        return findNames(event);
    }
}
