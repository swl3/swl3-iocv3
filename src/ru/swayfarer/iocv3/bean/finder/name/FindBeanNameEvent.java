package ru.swayfarer.iocv3.bean.finder.name;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.swl3.collections.list.ExtendedList;

public class FindBeanNameEvent<Target_Type> extends FindBeanPartEvent<ExtendedList<String>, Target_Type>{

    public FindBeanNameEvent(@NonNull Target_Type targetType, BeanFindingEvent beanFindingEvent)
    {
        super(targetType, beanFindingEvent);
    }
}
