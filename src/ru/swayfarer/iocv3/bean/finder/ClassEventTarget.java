package ru.swayfarer.iocv3.bean.finder;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ClassEventTarget {
    
    @NonNull
    public Class<?> type;
}
