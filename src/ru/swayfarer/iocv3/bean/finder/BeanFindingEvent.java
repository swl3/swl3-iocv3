package ru.swayfarer.iocv3.bean.finder;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.BeanInfo;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.context.Context;
import ru.swayfarer.swl3.collections.list.ExtendedList;

@Data
@Accessors(chain = true)
public class BeanFindingEvent {

    @NonNull
    public BeanInfo targetBeanInfo = new BeanInfo();

    @NonNull
    public ExtendedList<ContextBean> excludes = new ExtendedList<>();
    
    @NonNull
    public Context context;
    
    public Boolean beanRequired;
    
    public Object injectionTarget;
    
    public volatile ContextBean result;
    
    public BeanFinder beanFinder;
    
    public BeanFindingEvent copy()
    {
        var ret = new BeanFindingEvent(
                context
        );
        
        ret.setBeanRequired(beanRequired);
        ret.setInjectionTarget(injectionTarget);
        ret.setTargetBeanInfo(targetBeanInfo.copy());
        
        return ret;
    }
    
    public boolean isBeanRequired()
    {
        if (beanRequired != null)
        {
            return beanRequired;
        }
        
        return context.getContextSettings().isBeansRequiredByDefault();
    }
}
