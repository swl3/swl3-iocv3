package ru.swayfarer.iocv3.bean.finder;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

import java.lang.reflect.Parameter;

@Data
@Accessors(chain = true)
public class ParameterEventTarget {
    
    @NonNull
    public MethodEventTarget method;
    
    @NonNull
    public Parameter parameter;

    public int index;

    public ParameterEventTarget(@NonNull MethodEventTarget method, @NonNull Parameter parameter, int index)
    {
        this.method = method;
        this.parameter = parameter;
        this.index = index;
    }
}
