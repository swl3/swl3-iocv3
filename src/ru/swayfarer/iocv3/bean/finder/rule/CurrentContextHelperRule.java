package ru.swayfarer.iocv3.bean.finder.rule;

import lombok.var;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.Bean;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.bean.tags.IBeanTags;
import ru.swayfarer.iocv3.helper.ContextHelper;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class CurrentContextHelperRule extends AbstractBeanFindingRule{

	@NonNull
	public IFunction0<ContextHelper> contextHelper;
	
	public ContextBean contextBean = new ContextBean(this::getContextHelper);
	
	{
		contextBean.getInfo()
			.setBeanType(Bean.Singleton)
			.getCustomData()
				.add(IBeanTags.CurrentContext)
				.add(IBeanTags.GeneratedBean)
		;
	}
	
	@Override
	public void find(BeanFindingEvent event) 
	{
		var target = event.getTargetBeanInfo();
		
		if (target.getAccociatedType().getClassType() == ContextHelper.class && target.getCustomData().has(IBeanTags.CurrentContext))
		{
			event.setResult(contextBean);
		}
	}
	
	public ContextHelper getContextHelper()
	{
		return contextHelper.apply();
	}
}
