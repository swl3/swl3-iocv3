package ru.swayfarer.iocv3.bean.finder.rule;

import lombok.var;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.exception.NoSuchBeanFoundException;
import ru.swayfarer.swl3.exception.ExceptionsUtils;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class NoSuchBeanFoundRule extends AbstractBeanFindingRule {

    @Override
    public void find(BeanFindingEvent event)
    {
        ExceptionsUtils.If(
                event.getResult() == null && event.isBeanRequired(), 
                NoSuchBeanFoundException.class, 
                "Can't find bean for", event.getInjectionTarget(),"\nSearched bean info:", event.getTargetBeanInfo().toFancyString()
        );
    }
}
