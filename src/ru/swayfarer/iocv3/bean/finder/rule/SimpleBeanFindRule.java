package ru.swayfarer.iocv3.bean.finder.rule;

import lombok.var;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.util.Comparator;

@Data @EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SimpleBeanFindRule extends AbstractBeanFindingRule {
    
	public static ILogger logger = LogFactory.getLogger();
	
	public static boolean logFinding = false;
	
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @Override
    public void find(BeanFindingEvent event)
    {
    	log("Trying to find bean with names comparing...");
        ContextBean result = findBean(event, true);
        
        if (result == null)
        {
        	if (event.getTargetBeanInfo().isCanFindByType())
        		result = findBean(event, false);
        	else
        		log("Skipping find without name comparing because canFindByType is false!");
        }
        
        event.setResult(result);
    }

	public ContextBean findBean(BeanFindingEvent event, boolean isComparingNames) {
		var targetBeanInfo = event.getTargetBeanInfo();
        
        var reflectionsUtils = getReflectionsUtils();
        var context = event.getContext();
        var beans = context.getBeans().copy();

        if (event.getExcludes().isNotEmpty())
            beans.removeIf((bean) -> event.getExcludes().exStream().contains((b) -> b == bean));
        
        beans.sort(Comparator.comparingInt(bean -> bean.getInfo().getPriority()));

        var targetBeanClassType = targetBeanInfo.getAccociatedType().getClassType();
        var targetBeanNames = targetBeanInfo.getNames();
        
        var targetBeanTags = targetBeanInfo.getCustomData().data.exStream()
                .nonNull()
                .filter((e) -> e.getClass().equals(String.class))
                .toExList()
        ;
        
        log("Finding bean with tags", targetBeanTags);
        
        log("Now beans is:");
        logBeans(beans);
        
        if (isComparingNames)
    	{
    		beans = beans.filter((bean) -> bean.getInfo().getNames().containsSome(targetBeanNames));
    		
    		log("Filtered by name:");
            logBeans(beans);
    	}
        else
        {
            beans.filter((bean) -> bean.getInfo().isCanFindByType());
        	log("Names comparing skipped...");
        }
        
        int minOffset = Integer.MAX_VALUE;
        
        ContextBean result = null;
        
        for (var bean : beans) {
            var beanInfo = bean.getInfo();
            var beanAssociatedClass = beanInfo.getAccociatedType().getClassType();

            var beanTags = beanInfo.getCustomData().data.exStream()
                    .nonNull()
                    .filter((e) -> e.getClass().equals(String.class))
                    .toExList()
            ;
            
            if (beanTags.containsAll(targetBeanTags))
            {
                
                if (beanAssociatedClass.equals(targetBeanClassType))
                {
                	log("Found type equal bean: ", bean.getInfo().getNames().first(), "Returning it...");
                    result = bean;
                    break;
                }
                
                int offset = reflectionsUtils.types().getInheritanceOffset(beanAssociatedClass, targetBeanClassType);

                if (offset > -1 && offset < minOffset)
                {
                    minOffset = offset;
                    result = bean;
                    log("Found hierarchy bean: ", bean.getInfo().getNames().first(), "with offset", offset);
                }
                
                if (result != null)
                {
                	log("Returning hierarchy bean:", result.getInfo().getNames().first());
                }
            }
            else
            {
                log("Bean", bean.getInfo().getNames().first(), "does not contains all tags!");
            }
        }
		return result;
	}
	
	public static void logBeans(ExtendedList<ContextBean> contextBeans)
	{
		log(contextBeans.map((e) -> e.getInfo().getNames().first()));
	}
	
	public static void log(Object... text)
	{
		if (logFinding)
			logger.info(text);
	}
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
