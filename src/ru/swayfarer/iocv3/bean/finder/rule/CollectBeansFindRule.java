package ru.swayfarer.iocv3.bean.finder.rule;

import lombok.var;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.BeansList;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.bean.tags.IBeanTags;
import ru.swayfarer.swl3.collections.CollectionsSWL;

@Data @EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CollectBeansFindRule extends AbstractBeanFindingRule{
    
    @Override
    public void find(BeanFindingEvent event)
    {
        var targetBeanInfo = event.getTargetBeanInfo();
        var targetBeanClass = targetBeanInfo.getAccociatedType().getClassType();
        var context = event.getContext();
        
        if (targetBeanClass.equals(BeansList.class))
        {
            var generics = targetBeanInfo.getAccociatedType().getGenerics();
            var firstGeneric = generics.first();
            var contentBeanClass = firstGeneric.loadClass();
            var beanValues = new BeansList<>();
            
            CollectionsSWL.list(context.getBeans()).exStream()
                .filter((e) -> contentBeanClass.isAssignableFrom(e.getInfo().getAccociatedType().getClassType()))
                .filter((e) -> e.getInfo().getCustomData().has(IBeanTags.StandartBean))
                .map(ContextBean::getValue)
                .each(beanValues::add)
            ;
            
            var tmpBean = new ContextBean(() -> beanValues);
            
            event.setResult(tmpBean);
        }
    }
}
