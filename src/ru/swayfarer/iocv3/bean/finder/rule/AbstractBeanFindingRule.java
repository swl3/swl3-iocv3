package ru.swayfarer.iocv3.bean.finder.rule;

import lombok.var;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Data
@Accessors(chain = true)
public abstract class AbstractBeanFindingRule implements IFunction1NoR<BeanFindingEvent>{

    @Override
    public void applyNoRUnsafe(BeanFindingEvent event)
    {
        var result = event.getResult();
        
        if (result != null)
            return;
        
        find(event);
    }
    
    public abstract void find(BeanFindingEvent event);
}
