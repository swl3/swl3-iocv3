package ru.swayfarer.iocv3.bean.finder.rule;

import lombok.var;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.BeanType;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.LateBean;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.bean.tags.IBeanTags;

@Data @EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class LateBeanFindRule extends AbstractBeanFindingRule{

    @Override
    public void find(BeanFindingEvent event)
    {
        var contextFinder = event.getBeanFinder();
        
        var targetBeanInfo = event.getTargetBeanInfo();
        var targetBeanClass = targetBeanInfo.getAccociatedType().getClassType();
        
        if (targetBeanClass.equals(LateBean.class))
        {
            var generics = targetBeanInfo.getAccociatedType().getGenerics();
            var firstGeneric = generics.first();
            
            var contentEvent = event.copy();
            var contentBeanType = new BeanType()
                    .setClassType(firstGeneric.loadClass())
                    .setGenerics(firstGeneric.getChilds())
            ;
            
            contentEvent.getTargetBeanInfo().setAccociatedType(contentBeanType);
            var foundBean = contextFinder.findBean(contentEvent);

            if (foundBean != null && foundBean.getInfo().getCustomData().has(IBeanTags.StandartBean))
            {
                var tmpBean = new ContextBean(() -> {
                    return new LateBean<Object>() {
                        @Override
                        public Object applyUnsafe()
                        {
                            return foundBean.getValueFun().apply();
                        }
                    };
                });
                
                event.setResult(tmpBean);
            }
        }
    }
}
