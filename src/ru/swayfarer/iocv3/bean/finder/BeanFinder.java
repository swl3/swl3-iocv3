package ru.swayfarer.iocv3.bean.finder;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.BeanName;
import ru.swayfarer.iocv3.annotation.RequiredBean;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.finder.name.BeanNameFinder;
import ru.swayfarer.iocv3.bean.finder.name.FindNameByFieldEvent;
import ru.swayfarer.iocv3.bean.finder.name.FindNameByMethodParamEvent;
import ru.swayfarer.iocv3.bean.finder.rule.CollectBeansFindRule;
import ru.swayfarer.iocv3.bean.finder.rule.LateBeanFindRule;
import ru.swayfarer.iocv3.bean.finder.rule.NoSuchBeanFoundRule;
import ru.swayfarer.iocv3.bean.finder.rule.SimpleBeanFindRule;
import ru.swayfarer.iocv3.bean.finder.type.BeanTypeFinder;
import ru.swayfarer.iocv3.bean.finder.type.FindTypeByFieldEvent;
import ru.swayfarer.iocv3.bean.finder.type.FindTypeByMethodParamEvent;
import ru.swayfarer.iocv3.bean.tags.finder.BeanTagsFinder;
import ru.swayfarer.iocv3.context.finder.ContextFinder;
import ru.swayfarer.iocv3.context.finder.event.ContextByFieldEvent;
import ru.swayfarer.iocv3.context.finder.event.ContextByMethodParamEvent;
import ru.swayfarer.iocv3.exception.NoSuchBeanFoundException;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Getter
@Setter
@Accessors(chain = true)
public class BeanFinder {

    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @NonNull
    public IFunction0<ContextFinder> contextFinder;
    
    @NonNull
    public BeanNameFinder nameFinder = new BeanNameFinder();
    
    @NonNull
    public BeanTypeFinder typeFinder = new BeanTypeFinder();
    
    @NonNull
    public BeanTagsFinder tagsFinder = new BeanTagsFinder();
    
    @NonNull
    public IObservable<BeanFindingEvent> eventFind = Observables.createObservable()
            .exceptions().throwOnFail()
    ;

    public BeanFinder(@NonNull IFunction0<ReflectionsUtils> reflectionsUtils, @NonNull IFunction0<ContextFinder> contextFinder)
    {
        this.reflectionsUtils = reflectionsUtils;
        this.contextFinder = contextFinder;
    }

    public ContextFinder getContextFinder()
    {
        return contextFinder.apply();
    }
    
    public ContextBean findBean(Class<?> classOfMethod, Constructor<?> method, int paramIndex, Object instance)
    {
        var param = method.getParameters()[paramIndex];

    	var canFindByType = !getReflectionsUtils().annotations().findProperty()
    			.name("required")
    			.marker(BeanName.class)
    			.type(boolean.class)
    			.in(param)
    			.get()
		;
    	
        var paramTarget = new ParameterEventTarget(
                MethodEventTarget.readMethod(method, instance).setClassWithMethod(classOfMethod),
                param,
                paramIndex
        );
        
        var contextFinder = getContextFinder();
        var contextUtils = contextFinder.findContext((ContextByMethodParamEvent) new ContextByMethodParamEvent().setTarget(paramTarget));
        
        var event = new BeanFindingEvent(contextUtils.getContext());
        
        event
            .setInjectionTarget(param)
            .setBeanRequired(isBeanRequired(param.getAnnotations()))
            .setContext(contextUtils.getContext())
            .getTargetBeanInfo()
            	.setCanFindByType(canFindByType)
                .setCustomData(tagsFinder.findTags(classOfMethod, method, paramIndex, instance))
                .setNames(nameFinder.findNames(new FindNameByMethodParamEvent(paramTarget, event)))
                .setAccociatedType(typeFinder.findBeanType(new FindTypeByMethodParamEvent(paramTarget, event)))
        ;
        
        return findBean(event);
    }
    
    public ContextBean findBean(Class<?> classOfMethod, Method method, int paramIndex, Object instance)
    {
        var param = method.getParameters()[paramIndex];

    	var canFindByType = !getReflectionsUtils().annotations().findProperty()
    			.name("required")
    			.marker(BeanName.class)
    			.type(boolean.class)
    			.in(param)
    			.orElse(false)
		;
    	
        var paramTarget = new ParameterEventTarget(
                MethodEventTarget.readMethod(method, instance).setClassWithMethod(classOfMethod),
                param,
                paramIndex
        );
        
        var contextFinder = getContextFinder();
        var contextUtils = contextFinder.findContext((ContextByMethodParamEvent) new ContextByMethodParamEvent().setTarget(paramTarget));
        
        var event = new BeanFindingEvent(contextUtils.getContext());
        
        var findNames = nameFinder.findNames(new FindNameByMethodParamEvent(paramTarget, event));
        var findBeanType = typeFinder.findBeanType(new FindTypeByMethodParamEvent(paramTarget, event));
        
        event
            .setInjectionTarget(param)
            .setBeanRequired(isBeanRequired(param.getAnnotations()))
            .setContext(contextUtils.getContext())
            .getTargetBeanInfo()
            	.setCanFindByType(canFindByType)
                .setCustomData(tagsFinder.findTags(classOfMethod, method, paramIndex, instance))
                .setNames(findNames)
                .setAccociatedType(findBeanType)
        ;
        
        return findBean(event);
    }
    
    public ContextBean findBean(Class<?> classOfField, Field field, Object instance) 
    {
    	var canFindByType = !getReflectionsUtils().annotations().findProperty()
    			.name("required")
    			.marker(BeanName.class)
    			.type(boolean.class)
    			.in(field)
    			.optional()
    			.orElse(false)
		;
    	
        var fieldTarget = new FieldEventTarget(
                classOfField, 
                field
        ).setInstanceWithField(instance);
        
        var contextFinder = getContextFinder();
        var contextUtils = contextFinder.findContext((ContextByFieldEvent) new ContextByFieldEvent().setTarget(fieldTarget));
        
        var event = new BeanFindingEvent(contextUtils.getContext());
        
        var beanTags = tagsFinder.findTags(classOfField, field, instance);
        
        event
            .setInjectionTarget(field)
            .setBeanRequired(isBeanRequired(field.getAnnotations()))
            .setContext(contextUtils.getContext())
            .getTargetBeanInfo()
            	.setCanFindByType(canFindByType)
                .setCustomData(beanTags)
                .setNames(nameFinder.findNames(new FindNameByFieldEvent(fieldTarget, event)))
                .setAccociatedType(typeFinder.findBeanType(new FindTypeByFieldEvent(fieldTarget, event)))
        
        ;
        
        return findBean(event);
    }
    
    /**
     * Получить аргументы, которые будут переданы из контекста для вызова конструктора
     * @param classOfMethod Возвращаемый тип метода
     * @return Лист аргументов
     */
    public ExtendedList<Object> findArgs(Constructor<?> constructor, Class<?> classOfMethod)
    {
        ExtendedList<Object> invokeArgs = CollectionsSWL.list();
        var params = constructor.getParameters();

        var paramIndex = 0;
        for (Parameter param : params)
        {
            var bean = findBean(classOfMethod, constructor, paramIndex, null);
            Object contextElement = bean.getValue();
            invokeArgs.add(contextElement);
            paramIndex ++;
        }
        
        return invokeArgs;
    }
    
    /**
     * Получить аргументы, которые будут переданы из контекста для вызова метода
     * @param classOfMethod Возвращаемый тип метода
     * @return Лист аргументов
     */
    public ExtendedList<Object> findArgs(Object instance, Method method, Class<?> classOfMethod)
    {
        ExtendedList<Object> invokeArgs = CollectionsSWL.list();
        var params = method.getParameters();

        var paramIndex = 0;
        for (Parameter param : params)
        {
            var bean = findBean(classOfMethod, method, paramIndex, instance);
            
            if (bean == null)
            {
                throw new NoSuchBeanFoundException("Bean for param " + param.getName() + " in " + method + " not found!");
            }
            
            Object contextElement = bean.getValue();
            invokeArgs.add(contextElement);
            paramIndex ++;
        }
        
        return invokeArgs;
    }
    
    public ContextBean findBean(@NonNull BeanFindingEvent event) 
    {
        event.setBeanFinder(this);
        eventFind.next(event);
        return event.getResult();
    }
    
    public Boolean isBeanRequired(@NonNull Annotation[] annotations)
    {
        var reflectionsUtils = getReflectionsUtils();
        
        return reflectionsUtils.annotations().findProperty()
                .name("required")
                .type(boolean.class)
                .marker(RequiredBean.class)
                .in(annotations)
                .get()
        ;
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
    
    public void registerDefaultFuns()
    {
    	eventFind.subscribe().by(new LateBeanFindRule(), Priority.lateBeanRulePriority);
        eventFind.subscribe().by(new CollectBeansFindRule(), Priority.collectBeansRulePriority);
        eventFind.subscribe().by(new SimpleBeanFindRule(this::getReflectionsUtils), Priority.simpleRulePriority);
        eventFind.subscribe().by(new NoSuchBeanFoundRule(), Priority.noSuchBeanRulePriority);
    }
    
    {
        registerDefaultFuns();
    }
    
    public static class Priority {

        public static int currentContextRulePriority = 100;
        public static int lateBeanRulePriority = 100;
        public static int collectBeansRulePriority = lateBeanRulePriority + 100;
        public static int simpleRulePriority = collectBeansRulePriority + 100;
        public static int noSuchBeanRulePriority = Integer.MAX_VALUE;
    }
}
