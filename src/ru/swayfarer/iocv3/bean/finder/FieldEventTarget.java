package ru.swayfarer.iocv3.bean.finder;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

import java.lang.reflect.Field;

@Data
@Accessors(chain = true)
public class FieldEventTarget {
    public Object instanceWithField;

    @NonNull
    public Class<?> classWithField;

    @NonNull
    public Field field;
}
