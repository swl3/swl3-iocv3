package ru.swayfarer.iocv3.bean.finder.type;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.bean.finder.FieldEventTarget;

public class FindTypeByFieldEvent extends FindTypeEvent<FieldEventTarget> {

    public FindTypeByFieldEvent(@NonNull FieldEventTarget targetType, @NonNull BeanFindingEvent beanFindingEvent)
    {
        super(targetType, beanFindingEvent);
    }
}
