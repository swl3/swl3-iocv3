package ru.swayfarer.iocv3.bean.finder.type;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.bean.finder.ClassEventTarget;

public class FindTypeByClassEvent extends FindTypeEvent<ClassEventTarget>{

    public FindTypeByClassEvent(@NonNull ClassEventTarget targetType, BeanFindingEvent beanFindingEvent)
    {
        super(targetType, beanFindingEvent);
    }
}
