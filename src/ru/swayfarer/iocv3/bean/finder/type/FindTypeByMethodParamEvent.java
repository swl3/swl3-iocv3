package ru.swayfarer.iocv3.bean.finder.type;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.bean.finder.ParameterEventTarget;

public class FindTypeByMethodParamEvent extends FindTypeEvent<ParameterEventTarget>{

    public FindTypeByMethodParamEvent(@NonNull ParameterEventTarget targetType, @NonNull BeanFindingEvent beanFindingEvent)
    {
        super(targetType, beanFindingEvent);
    }
}
