package ru.swayfarer.iocv3.bean.finder.type;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.iocv3.bean.BeanType;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.bean.finder.name.FindBeanPartEvent;

public class FindTypeEvent<Target_Type> extends FindBeanPartEvent<BeanType, Target_Type>{

    public FindTypeEvent(@NonNull Target_Type targetType, BeanFindingEvent beanFindingEvent)
    {
        super(targetType, beanFindingEvent);
    }
}
