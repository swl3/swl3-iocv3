package ru.swayfarer.iocv3.bean.finder.type;

import lombok.var;
import java.lang.reflect.Method;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.BeanType;
import ru.swayfarer.iocv3.bean.finder.ClassEventTarget;
import ru.swayfarer.iocv3.bean.finder.MethodEventTarget;
import ru.swayfarer.iocv3.bean.finder.type.rules.AnnotationsBeanTypeRule;
import ru.swayfarer.iocv3.bean.finder.type.rules.ClassBeanTypeRule;
import ru.swayfarer.iocv3.bean.finder.type.rules.FieldBeanTypeRule;
import ru.swayfarer.iocv3.bean.finder.type.rules.MethodBeanTypeRule;
import ru.swayfarer.iocv3.bean.finder.type.rules.ParamBeanTypeRule;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class BeanTypeFinder {

    public FunctionsChain1<FindTypeByClassEvent, BeanType> findByClassFuns = new FunctionsChain1<>();
    public FunctionsChain1<FindTypeByFieldEvent, BeanType> findByFieldsFuns = new FunctionsChain1<>();
    public FunctionsChain1<FindTypeByMethodEvent, BeanType> findByMethodFuns = new FunctionsChain1<>();
    public FunctionsChain1<FindTypeByMethodParamEvent, BeanType> findByParamFuns = new FunctionsChain1<>();
    
    public BeanType findBeanType(FindTypeByMethodParamEvent event) 
    {
        return findByParamFuns.apply(event);
    }

    public BeanType findBeanType(FindTypeByFieldEvent event)
    {
        return findByFieldsFuns.apply(event);
    }
    
    public BeanType findBeanType(FindTypeByClassEvent event)
    {
        return findByClassFuns.apply(event);
    }
    
    public BeanType findBeanType(FindTypeByMethodEvent event)
    {
        return findByMethodFuns.apply(event);
    }
    
    public BeanType findBeanType(Class<?> classToScan)
    {
        var classTarget = new ClassEventTarget(classToScan);
        return findBeanType(new FindTypeByClassEvent(classTarget, null));
    }
    
    public BeanType findBeanType(Class<?> classOfMethod, Method method, Object instance)
    {
        var methodEvent = new FindTypeByMethodEvent(
                MethodEventTarget.readMethod(method, instance).setClassWithMethod(classOfMethod),
                null
        );
        
        return findBeanType(methodEvent);
    }
}
