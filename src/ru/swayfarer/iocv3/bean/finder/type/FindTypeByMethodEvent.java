package ru.swayfarer.iocv3.bean.finder.type;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.bean.finder.MethodEventTarget;

public class FindTypeByMethodEvent extends FindTypeEvent<MethodEventTarget>{

    public FindTypeByMethodEvent(@NonNull MethodEventTarget targetType, BeanFindingEvent beanFindingEvent)
    {
        super(targetType, beanFindingEvent);
    }
}
