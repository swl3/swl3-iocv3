package ru.swayfarer.iocv3.bean.finder.type.rules;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.BeanType;
import ru.swayfarer.iocv3.bean.finder.type.FindTypeByMethodParamEvent;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class ParamBeanTypeRule implements IFunction1<FindTypeByMethodParamEvent, BeanType> {

    @NonNull
    public IFunction0<AnnotationsBeanTypeRule> annotationsBeanTypeRule;
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @Override
    public BeanType applyUnsafe(FindTypeByMethodParamEvent event)
    {
        var reflectionsUtils = getReflectionsUtils();
        var annotationsBeanTypeRule = getAnnotationsBeanTypeRule();
        var param = event.getTargetType().getParameter();
        var ret = annotationsBeanTypeRule.apply(param.getAnnotations());
        
        if (ret != null)
            return ret;
        
        ret = new BeanType()
                .setClassType(param.getType())
                .setGenerics(reflectionsUtils.generics().of(param))
        ;
        
        return ret;
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
    
    public AnnotationsBeanTypeRule getAnnotationsBeanTypeRule()
    {
        return annotationsBeanTypeRule.apply();
    }
}
