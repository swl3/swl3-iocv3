package ru.swayfarer.iocv3.bean.finder.type.rules;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.BeanType;
import ru.swayfarer.iocv3.bean.finder.type.FindTypeByFieldEvent;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class FieldBeanTypeRule implements IFunction1<FindTypeByFieldEvent, BeanType> {

    @NonNull
    public IFunction0<AnnotationsBeanTypeRule> annotationsBeanTypeRule;
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @Override
    public BeanType applyUnsafe(FindTypeByFieldEvent event)
    {
        var annotationsBeanTypeRule = getAnnotationsBeanTypeRule();
        var field = event.getTargetType().getField();
        var ret = annotationsBeanTypeRule.apply(field.getAnnotations());
        var reflectionsUtils = getReflectionsUtils();
        var fieldGenerics = reflectionsUtils.generics().of(field);
        
        if (ret != null)
            return ret;
        
        var fieldType = field.getType();
        
        ret = new BeanType()
                .setClassType(fieldType)
                .setGenerics(fieldGenerics)
        ;
        
        return ret;
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
    
    public AnnotationsBeanTypeRule getAnnotationsBeanTypeRule()
    {
        return annotationsBeanTypeRule.apply();
    }
}
