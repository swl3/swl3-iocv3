package ru.swayfarer.iocv3.bean.finder.type.rules;

import lombok.var;
import java.lang.annotation.Annotation;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.Bean;
import ru.swayfarer.iocv3.bean.BeanType;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class AnnotationsBeanTypeRule {

    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    public BeanType apply(Annotation[] annotations)
    {
        var reflectionsUtils = getReflectionsUtils();
        var foundType = reflectionsUtils.annotations().findProperty()
            .marker(Bean.class)
            .type(Class.class)
            .name("associatedType")
            .in(annotations)
            .get()
        ;
        
        return foundType == null || foundType == Object.class ? null : BeanType.of(foundType);
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
