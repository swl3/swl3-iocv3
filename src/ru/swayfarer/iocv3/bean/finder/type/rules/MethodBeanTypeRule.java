package ru.swayfarer.iocv3.bean.finder.type.rules;

import lombok.var;
import java.lang.annotation.Annotation;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.BeanType;
import ru.swayfarer.iocv3.bean.finder.type.FindTypeByMethodEvent;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class MethodBeanTypeRule implements IFunction1<FindTypeByMethodEvent, BeanType> {

    @NonNull
    public IFunction0<AnnotationsBeanTypeRule> annotationsBeanTypeRule;
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @Override
    public BeanType applyUnsafe(FindTypeByMethodEvent event)
    {
        var reflectionsUtils = getReflectionsUtils();
        var annotationsBeanTypeRule = getAnnotationsBeanTypeRule();
        var targetType = event.getTargetType();
        var annotations = targetType.getAnnotations().toArray(Annotation.class);
        
        var ret = annotationsBeanTypeRule.apply(annotations);
        
        if (ret != null)
            return ret;
        
        ret = new BeanType()
                .setClassType(targetType.getReturnType())
                .setGenerics(reflectionsUtils.generics().of(targetType.getGenericReturnType()))
        ;
        
        return ret;
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
    
    public AnnotationsBeanTypeRule getAnnotationsBeanTypeRule()
    {
        return annotationsBeanTypeRule.apply();
    }
}
