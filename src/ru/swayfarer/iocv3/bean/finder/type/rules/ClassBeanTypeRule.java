package ru.swayfarer.iocv3.bean.finder.type.rules;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.BeanType;
import ru.swayfarer.iocv3.bean.finder.type.FindTypeByClassEvent;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class ClassBeanTypeRule implements IFunction1<FindTypeByClassEvent, BeanType> {

    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @NonNull
    public IFunction0<AnnotationsBeanTypeRule> annotationsBeanTypeRule;
    
    @Override
    public BeanType applyUnsafe(FindTypeByClassEvent event)
    {
        var reflectionsUtils = getReflectionsUtils();
        
        var annotationsBeanTypeRule = getAnnotationsBeanTypeRule();
        var type = event.getTargetType().getType();
        var ret = annotationsBeanTypeRule.apply(type.getAnnotations());
        
        if (ret != null)
            return ret;
        
        var generics = reflectionsUtils.generics().ofParent(type);
        
        ret = new BeanType()
                .setClassType(type)
                .setGenerics(generics)
        ;
        
        return ret;
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
    
    public AnnotationsBeanTypeRule getAnnotationsBeanTypeRule()
    {
        return annotationsBeanTypeRule.apply();
    }
}
