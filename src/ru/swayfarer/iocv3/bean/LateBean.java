package ru.swayfarer.iocv3.bean;

import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;

public interface LateBean <Ret_Type> extends IFunction0<Ret_Type> {}
