package ru.swayfarer.iocv3.bean;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.iocv3.context.beanhandlers.BeanHandlingTrace;
import ru.swayfarer.iocv3.exception.BeanInitializationException;
import ru.swayfarer.iocv3.exception.IocException;
import ru.swayfarer.iocv3.exception.NoSuchBeanFoundException;
import ru.swayfarer.swl3.collections.set.WeakHashSet;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;

import java.util.Collections;
import java.util.Set;
import java.util.UUID;

@Data
@Accessors(chain = true)
public class ContextBean {

    public UUID debugUUID = UUID.randomUUID();

    @NonNull
    public Set<Object> allCreatedValues = Collections.synchronizedSet(new WeakHashSet<>());

    @NonNull
    public BeanInfo info = new BeanInfo();
    
    @NonNull
    public IFunction0<Object> valueFun;
    
    public Object getValue() 
    {
        var ret = executeValueFun(valueFun, this);
        
        allCreatedValues.add(ret);
        
        return ret;
    }
    
    public static Object executeValueFun(@NonNull ContextBean contextBean)
    {
        return executeValueFun(contextBean.getValueFun(), contextBean);
    }
    
    public static Object executeValueFun(@NonNull IFunction0<Object> valueFun, ContextBean bean)
    {
        BeanHandlingTrace.trace("Getting bean value of", bean.getInfo().getNames());

        try
        {
            var ret = valueFun.apply();
            return ret;
        }
        catch (NoSuchBeanFoundException e)
        {
            throw e;
        }
        catch (Throwable e)
        {
            if (e instanceof IocException)
                throw e;
            
            var ex = new BeanInitializationException("Can't initialize bean " + bean.getInfo().getNames() + " of " + bean.getInfo().getAccociatedType().getClassType(), e);
            ExceptionsUtils.throwException(ex);
            return null;
        }
    }
}
