package ru.swayfarer.iocv3.bean;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.Bean;
import ru.swayfarer.iocv3.bean.tags.BeanCustomData;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

import java.util.concurrent.atomic.AtomicInteger;

@Data
@Accessors(chain = true)
public class BeanInfo {
    
    @NonNull
    public BeanCustomData customData = new BeanCustomData();
    
    @NonNull
    public ExtendedList<String> names = new ExtendedList<>();
    
    @NonNull
    public String beanType = Bean.Singleton;
    
    @NonNull 
    public BeanType accociatedType = BeanType.objectType;
    
    public int priority;
   
    public boolean canFindByType;
    
    public Boolean lazy;
    
    public BeanInfo data(@NonNull Object tag)
    {
        customData.add(tag);
        return this;
    }
    
    public BeanInfo data(@NonNull BeanCustomData data)
    {
        customData.addAll(data);
        return this;
    }
    
    
    public BeanInfo name(Object name)
    {
        names.addExclusive(String.valueOf(name));
        return this;
    }
    
    public BeanInfo copy()
    {
        var ret = new BeanInfo();
        
        ret.setNames(names.copy());
        ret.setBeanType(beanType);
        ret.setAccociatedType(accociatedType.copy());
        ret.setLazy(lazy);
        ret.setCanFindByType(canFindByType);
        
        return ret;
    }
    
    public boolean isLazy()
    {
        return Boolean.TRUE.equals(lazy);
    }

    public String toFancyString()
    {
        var str = new DynamicString();
        var indent = new AtomicInteger(0);
        IFunction0NoR newline = () -> {
            str.newLine().indent(indent.get());
        };

        str.append("{");
        indent.incrementAndGet();
        newline.apply();

        accociatedType.toFancyString(str, indent.get());
        customData.toFancyString(str, indent.get());

        str.append("names: ");
        str.append(names);

        newline.apply();

        str.append("type: ");
        str.append(beanType);

        newline.apply();

        str.append("lazy: ");
        str.append(StringUtils.orIfEmpty(lazy, "<not presented>"));

        newline.apply();

        str.append("typed (canFindByType): ");
        str.append(canFindByType);

        indent.decrementAndGet();
        newline.apply();
        str.append("}");
        newline.apply();

        return str.toString();
    }
}
