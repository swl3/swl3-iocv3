package ru.swayfarer.iocv3.bean.tags.finder.event;

import lombok.var;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.finder.FieldEventTarget;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class FindBeanTagsByFieldEvent extends FindBeanTagsEvent<FieldEventTarget>{}
