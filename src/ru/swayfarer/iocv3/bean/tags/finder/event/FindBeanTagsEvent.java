package ru.swayfarer.iocv3.bean.tags.finder.event;

import lombok.var;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.tags.BeanCustomData;
import ru.swayfarer.iocv3.bean.tags.finder.BeanTagsFinder;
import ru.swayfarer.swl3.observable.event.AbstractCancelableEvent;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class FindBeanTagsEvent<Target_Type> extends AbstractCancelableEvent {
    
    public BeanCustomData result = new BeanCustomData();
    public Target_Type target;
    public BeanTagsFinder tagsFinder;
}
