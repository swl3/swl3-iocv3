package ru.swayfarer.iocv3.bean.tags.finder;

import lombok.var;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.finder.ClassEventTarget;
import ru.swayfarer.iocv3.bean.finder.FieldEventTarget;
import ru.swayfarer.iocv3.bean.finder.MethodEventTarget;
import ru.swayfarer.iocv3.bean.finder.ParameterEventTarget;
import ru.swayfarer.iocv3.bean.tags.BeanCustomData;
import ru.swayfarer.iocv3.bean.tags.finder.event.FindBeanTagsByClassEvent;
import ru.swayfarer.iocv3.bean.tags.finder.event.FindBeanTagsByFieldEvent;
import ru.swayfarer.iocv3.bean.tags.finder.event.FindBeanTagsByMethodEvent;
import ru.swayfarer.iocv3.bean.tags.finder.event.FindBeanTagsByMethodParamEvent;
import ru.swayfarer.iocv3.bean.tags.finder.rules.AnnotationsBeanTagsFindRule;
import ru.swayfarer.iocv3.bean.tags.finder.rules.ClassBeanTagsFindRule;
import ru.swayfarer.iocv3.bean.tags.finder.rules.FieldBeanTagsFindRule;
import ru.swayfarer.iocv3.bean.tags.finder.rules.MethodBeanTagsFindRule;
import ru.swayfarer.iocv3.bean.tags.finder.rules.ParamBeanTagsFindRule;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class BeanTagsFinder {

    public IObservable<FindBeanTagsByClassEvent> eventFindByClass = Observables.createObservable();
    public IObservable<FindBeanTagsByFieldEvent> eventFindByField = Observables.createObservable();
    public IObservable<FindBeanTagsByMethodEvent> eventFindByMethod = Observables.createObservable();
    public IObservable<FindBeanTagsByMethodParamEvent> eventFindByParam = Observables.createObservable();
    
    public BeanCustomData findTags(@NonNull FindBeanTagsByClassEvent evt)
    {
        evt.setTagsFinder(this);
        eventFindByClass.next(evt);
        return evt.getResult();
    }
    
    public BeanCustomData findTags(@NonNull FindBeanTagsByFieldEvent evt)
    {
        evt.setTagsFinder(this);
        eventFindByField.next(evt);
        return evt.getResult();
    }
    
    public BeanCustomData findTags(@NonNull FindBeanTagsByMethodParamEvent evt)
    {
        evt.setTagsFinder(this);
        eventFindByParam.next(evt);
        return evt.getResult();
    }
    
    public BeanCustomData findTags(@NonNull FindBeanTagsByMethodEvent evt)
    {
        evt.setTagsFinder(this);
        eventFindByMethod.next(evt);
        return evt.getResult();
    }

    /* 
     * -----------------------------------------------------------------------
     *                                Хелперы
     * -----------------------------------------------------------------------
     */
    
    public BeanCustomData findTags(@NonNull Class<?> cl)
    {
        var target = new ClassEventTarget(cl);
        var event = (FindBeanTagsByClassEvent) new FindBeanTagsByClassEvent().setTarget(target);
        return findTags(event);
    }
    
    public BeanCustomData findTags(@NonNull Class<?> classWithField, @NonNull Field field, @NonNull Object instance)
    {
        var target = (FieldEventTarget) new FieldEventTarget(classWithField, field)
                .setInstanceWithField(instance)
        ;
        
        var event = (FindBeanTagsByFieldEvent) new FindBeanTagsByFieldEvent()
                .setTarget(target)
        ;
        
        return findTags(event);
    }
    
    public BeanCustomData findTags(@NonNull Class<?> classWithMethod, @NonNull Method method, @NonNull Object instance)
    {
        var target = MethodEventTarget.readMethod(method, instance).setClassWithMethod(classWithMethod);
        
        var event = (FindBeanTagsByMethodEvent) new FindBeanTagsByMethodEvent()
                .setTarget(target)
        ;
        
        return findTags(event);
    }
    
    public BeanCustomData findTags(@NonNull Class<?> classWithMethod, @NonNull Method method, int paramIndex, @NonNull Object instance)
    {
        var param = method.getParameters()[paramIndex];

        var methodTarget = MethodEventTarget.readMethod(method, instance).setClassWithMethod(classWithMethod);
        var paramTarget = new ParameterEventTarget(methodTarget, param, paramIndex);

        var event = (FindBeanTagsByMethodParamEvent) new FindBeanTagsByMethodParamEvent()
                .setTarget(paramTarget)
        ;
        
        return findTags(event);
    }
    
    public BeanCustomData findTags(@NonNull Class<?> classWithMethod, @NonNull Constructor<?> method, int paramIndex, @NonNull Object instance)
    {
        var param = method.getParameters()[paramIndex];

        var methodTarget = MethodEventTarget.readMethod(method, instance);
        var paramTarget = new ParameterEventTarget(methodTarget, param, paramIndex);

        var event = (FindBeanTagsByMethodParamEvent) new FindBeanTagsByMethodParamEvent()
                .setTarget(paramTarget)
        ;
        
        return findTags(event);
    }
}
