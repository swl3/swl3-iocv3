package ru.swayfarer.iocv3.bean.tags.finder.rules;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import java.lang.annotation.Annotation;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.BeanTag;
import ru.swayfarer.iocv3.annotation.CollectTags;
import ru.swayfarer.iocv3.bean.tags.BeanCustomData;
import ru.swayfarer.iocv3.bean.tags.finder.BeanTagFindingState;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.FilteringList;

@Getter
@Setter
@Accessors(chain = true)
public class AnnotationsBeanTagsFindRule {

    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;

    public AnnotationsBeanTagsFindRule(@NonNull IFunction0<ReflectionsUtils> reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
    }

    public boolean findTags(@NonNull Annotation[] annotations, @NonNull BeanCustomData beanTags)
    {
        var reflectionsUtils = getReflectionsUtils();
        
        var foundTags = new ExtendedList<String>();
        
        var filtering = reflectionsUtils.annotations().findProperty()
            .name("filter")
            .type(String[].class)
            .marker(CollectTags.class)
            .in(annotations)
            .optional().orElse(new String[0])
        ;
        
        var collectSuper = reflectionsUtils.annotations().findProperty()
            .name("collectSuper")
            .type(boolean.class)
            .marker(CollectTags.class)
            .in(annotations)
            .optional().orElse(false)
        ;
        
        var collectParent = reflectionsUtils.annotations().findProperty()
            .name("collectParent")
            .type(boolean.class)
            .marker(CollectTags.class)
            .in(annotations)
            .orElse(false)
        ;
        
        if (collectSuper)
        {
            var tags = reflectionsUtils.annotations().findProperty()
                .name("value")
                .type(String[].class)
                .marker(BeanTag.class)
                .in(annotations)
                .get()
            ;
            
            foundTags.addAll(tags);
        }
        else
        {
            var tags = reflectionsUtils.annotations().findAnnotation()
                    .ofType(BeanTag.class)
                    .in(annotations)
                    .getAll()
            ;
            
            if (tags != null)
            {
            	for (var tag : tags)
                {
                    var value = tag.value();
                    foundTags.addAll(value);
                }
            }
        }
        
        beanTags.data.addAll(foundTags);
        
        var filter = FilteringList.of(CollectionsSWL.iterable(filtering));
        
        foundTags
            .distinct()
            .filter(filter::isMatches)
        ;
        
        var ret = new BeanTagFindingState();
        
        ret.getCollectState()
            .setCollectParents(collectParent)
            .setCollectSuper(collectSuper)
        ;


        return collectParent;
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
