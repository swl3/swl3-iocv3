package ru.swayfarer.iocv3.bean.tags.finder.rules;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.tags.finder.event.FindBeanTagsByClassEvent;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ClassBeanTagsFindRule extends AbstractBeanTypesFindingRule<FindBeanTagsByClassEvent> {

    @NonNull
    public IFunction0<AnnotationsBeanTagsFindRule> annotationsRule;

    public ClassBeanTagsFindRule(@NonNull IFunction0<AnnotationsBeanTagsFindRule> annotationsRule)
    {
        this.annotationsRule = annotationsRule;
    }

    @Override
    public void find(FindBeanTagsByClassEvent event)
    {
        var target = event.getTarget();
        
        var annotationsRule = getAnnotationsRule();
        
        var result = event.getResult();
        annotationsRule.findTags(target.getType().getAnnotations(), result);
        
        event.setCanceled(true);
    }

    public AnnotationsBeanTagsFindRule getAnnotationsRule()
    {
        return annotationsRule.apply();
    }
}
