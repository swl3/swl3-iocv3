package ru.swayfarer.iocv3.bean.tags.finder.rules;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.tags.finder.event.FindBeanTagsByMethodParamEvent;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;

@Getter
@Setter
@Accessors(chain = true)
public class ParamBeanTagsFindRule extends AbstractBeanTypesFindingRule<FindBeanTagsByMethodParamEvent> {

    @NonNull
    public IFunction0<AnnotationsBeanTagsFindRule> annotationsRule;

    public ParamBeanTagsFindRule(@NonNull IFunction0<AnnotationsBeanTagsFindRule> annotationsRule)
    {
        this.annotationsRule = annotationsRule;
    }

    @Override
    public void find(FindBeanTagsByMethodParamEvent event)
    {
        var target = event.getTarget();
        
        var annotationsRule = getAnnotationsRule();
        var param = target.getParameter();
        
        var result = event.getResult();
        annotationsRule.findTags(param.getAnnotations(), result);
        
        event.setCanceled(true);
    }

    public AnnotationsBeanTagsFindRule getAnnotationsRule()
    {
        return annotationsRule.apply();
    }
}
