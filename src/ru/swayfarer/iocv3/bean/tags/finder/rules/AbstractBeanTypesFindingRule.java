package ru.swayfarer.iocv3.bean.tags.finder.rules;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Getter
@Setter
@Accessors(chain = true)
public abstract class AbstractBeanTypesFindingRule<Event_Type> implements IFunction1NoR<Event_Type>{
    
    @Override
    public void applyNoRUnsafe(Event_Type event)
    {
        find(event);
    }
    
    public abstract void find(Event_Type event);
}
