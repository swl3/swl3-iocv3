package ru.swayfarer.iocv3.bean.tags.finder.rules;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import java.lang.annotation.Annotation;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.tags.finder.event.FindBeanTagsByMethodEvent;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;

@Getter
@Setter
@Accessors(chain = true)
public class MethodBeanTagsFindRule extends AbstractBeanTypesFindingRule<FindBeanTagsByMethodEvent> {

    @NonNull
    public IFunction0<AnnotationsBeanTagsFindRule> annotationsRule;

    public MethodBeanTagsFindRule(@NonNull IFunction0<AnnotationsBeanTagsFindRule> annotationsRule)
    {
        this.annotationsRule = annotationsRule;
    }

    @Override
    public void find(FindBeanTagsByMethodEvent event)
    {
        var target = event.getTarget();
        
        var annotationsRule = getAnnotationsRule();
        
        var result = event.getResult();
        var collectParents = annotationsRule.findTags(target.getAnnotations().toArray(Annotation.class), result);
        
        event.setCanceled(true);
        
        if (!collectParents)
            return;

        var tagsFinder = event.getTagsFinder();
        var parentTags = tagsFinder.findTags(event.getTarget().getClassWithMethod());
        
        if (parentTags != null)
        {
            result.addAll(parentTags);
        }
    }

    public AnnotationsBeanTagsFindRule getAnnotationsRule()
    {
        return annotationsRule.apply();
    }
}
