package ru.swayfarer.iocv3.bean.tags.finder.rules;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import java.util.Arrays;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.tags.finder.event.FindBeanTagsByFieldEvent;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;

@Getter
@Setter
@Accessors(chain = true)
public class FieldBeanTagsFindRule extends AbstractBeanTypesFindingRule<FindBeanTagsByFieldEvent> {

    @NonNull
    public IFunction0<AnnotationsBeanTagsFindRule> annotationsRule;

    public FieldBeanTagsFindRule(@NonNull IFunction0<AnnotationsBeanTagsFindRule> annotationsRule)
    {
        this.annotationsRule = annotationsRule;
    }

    @Override
    public void find(FindBeanTagsByFieldEvent event)
    {
        var target = event.getTarget();
        
        var annotationsRule = getAnnotationsRule();
        var field = target.getField();
        
        var result = event.getResult();
        var collectParents = annotationsRule.findTags(field.getAnnotations(), result);
        
        event.setCanceled(true);
        
        if (!collectParents)
        {
        	return;
        }

        var tagsFinder = event.getTagsFinder();
        var parentTags = tagsFinder.findTags(event.getTarget().getClassWithField());
        
        if (parentTags != null)
        {
            result.addAll(parentTags);
        }
    }

    public AnnotationsBeanTagsFindRule getAnnotationsRule()
    {
        return annotationsRule.apply();
    }
}
