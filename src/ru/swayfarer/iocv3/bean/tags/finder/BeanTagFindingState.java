package ru.swayfarer.iocv3.bean.tags.finder;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class BeanTagFindingState {
    
    @NonNull
    public CollectState collectState = new CollectState();
    
    @Data
    @Accessors(chain = true)
    public static class CollectState {
        public boolean collectSuper = true;
        public boolean collectParents = true;
    }
}
