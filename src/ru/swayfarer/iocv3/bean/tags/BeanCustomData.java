package ru.swayfarer.iocv3.bean.tags;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Data
@Accessors(chain = true)
public class BeanCustomData {

    public ExtendedList<Object> data = new ExtendedList<>().synchronyzed();
    public ExtendedMap<String, Object> dataMap = new ExtendedMap<>().synchronize();

    {
        dataMap.put("tags", data);
    }
    
    public BeanCustomData add(@NonNull Object entry)
    {
        if (!has(entry))
            data.add(entry);
        
        return this;
    }
    
    public boolean has(@NonNull Object entry)
    {
        return data.contains(entry);
    }
    
    public BeanCustomData addAll(@NonNull BeanCustomData data)
    {
        this.data.addAll(data.data);
        this.data.distinct();
        
        return this;
    }

    public BeanCustomData add(@NonNull String key, Object value)
    {
        dataMap.put(key, value);
        return this;
    }

    public boolean isEmpty()
    {
        return data.isEmpty() && ((dataMap.containsKey("tags") && dataMap.size() == 1) || dataMap.isEmpty());
    }

    public <T> T get(@NonNull String key)
    {
        return dataMap.getValue(key);
    }

    public <T> T getOrAdd(@NonNull String key, @NonNull GeneratedFuns.IFunction0<T> newObjFun)
    {
        return dataMap.getOrCreate(key, newObjFun);
    }

    public void toFancyString(DynamicString str, int indentValue)
    {
        var indent = new AtomicInteger(indentValue);
        IFunction0NoR newline = () -> {
            str.newLine().indent(indent.get());
        };

        str.append("custom-data: ");

        if (isEmpty())
        {
            str.append("<empty>");
            newline.apply();
            return;
        }

        str.append(" {");
        indent.incrementAndGet();

        newline.apply();
        str.append("tags: ");
        str.append(data);

        newline.apply();
        str.append("data: ");
        var dataMap = new HashMap<>(this.dataMap).remove("tags");
        str.append(dataMap);

        indent.decrementAndGet();
        newline.apply();

        str.append("}");
        newline.apply();
    }
}
