package ru.swayfarer.iocv3.bean.tags;

public interface IBeanTags {
    public String GeneratedBean = "#generated";
    public String StandartBean = "#standart";
    public String CanBeSkipedAtCircularDependency = "#canBeSkipedAtCircularDependency";
    public String FromComponentScan = "#fromComponentScan";
    public String FromSourceObject = "#fromSourceObject";
    public String FromProperty = "#fromPropertySwconf2";
    public String CurrentContext = "#currentContext";
    public String Logging = "#logger-swl3";
    public String Temp = "#tempolary";

    public static interface BeanExceptions {
        public String prefix = "swl3-iocv3-";

        public String iocv3 = prefix + "exception";

        public String tagContextSourceScan = prefix + "#contextSourceScan";
        public String tagBeanClassScan = prefix + "#beanClassScan";
        public String tagClassScan = prefix + "#classScan";

        public String tagAddBean = prefix + "#addBean";

        public String keyScannedContextBeanClassName = prefix + "scanned-bean";
        public String keyScannedContextSourceClassName = prefix + "scanned-source";
        public String keyTargetContext = prefix + "target-context";
    }
}
