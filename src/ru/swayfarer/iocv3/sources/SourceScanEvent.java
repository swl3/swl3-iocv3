package ru.swayfarer.iocv3.sources;

import lombok.var;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.context.Context;
import ru.swayfarer.iocv3.helper.ContextHelper;

@Data
@NoArgsConstructor 
@Accessors(chain = true)
public class SourceScanEvent {
    
    @NonNull
    public Object instance;
    
    @NonNull
    public ContextSourceScanner sourceScanner;
    
    @NonNull
    public Context context;
    
    @NonNull
    public ContextBean result;
    
    @NonNull
    public ContextHelper contextHelper;
}
