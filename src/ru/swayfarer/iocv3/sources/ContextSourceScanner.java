package ru.swayfarer.iocv3.sources;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.context.finder.ContextFinder;
import ru.swayfarer.iocv3.helper.ContextHelper;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;

@Data
@Accessors(chain = true)
public class ContextSourceScanner {
    
    public IObservable<SourceScanEvent> eventScan = Observables.createObservable();
    
    public ExtendedList<IFunction0<IFunction0<ContextBean>>> beanCreationFunFuns = new ExtendedList<>();
    
    @NonNull
    public IFunction0<ContextHelper> contextHelper;

    public void scan(@NonNull Object source)
    {
        var contextFinder = getContextFinder();
        
        var contextScanEvent = new SourceScanEvent()
                .setInstance(source)
                .setSourceScanner(this)
                .setContextHelper(contextHelper.apply())
                .setContext(contextFinder.findContext(source.getClass()).getContext())
        ;        
        eventScan.next(contextScanEvent);
    }

    public ContextFinder getContextFinder()
    {
        return contextHelper.apply().getContextFinder();
    }
}
