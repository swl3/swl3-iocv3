package ru.swayfarer.iocv3.sources;

import lombok.var;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.BeanProcessor;
import ru.swayfarer.iocv3.annotation.BeanValuesProcessor;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.context.beanhandlers.BeanProcessorsHandler;
import ru.swayfarer.iocv3.context.finder.ContextFinder;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class BeanProcessorsRule implements GeneratedFuns.IFunction1NoR<SourceScanEvent> {

    @NonNull
    public GeneratedFuns.IFunction0<ReflectionsUtils> reflectionsUtils;

    @NonNull
    public GeneratedFuns.IFunction0<ContextFinder> contextFinder;

    @NonNull
    public GeneratedFuns.IFunction0<ExceptionsHandler> exceptionsHandler;

    @Override
    public void applyNoRUnsafe(SourceScanEvent event) throws Throwable {
        var instance = event.getInstance();
        var reflectionsUtils = getReflectionsUtils();
        var contextFinder = getContextFinder();
        var exceptionsHandler = getExceptionsHandler();

        var methodFilters = reflectionsUtils.methods().filters();

        var methods = reflectionsUtils.methods().stream(instance)
                .filter(methodFilters.annotation().assignable(BeanProcessor.class))
                .not().filter(methodFilters.mod().statics())
                .not().filter(methodFilters.mod().abstracts())
                .filter(methodFilters.params().count().equal(1))
                .filter(methodFilters.params().canAcceptTypes(ContextBean.class))
                .toExList()
        ;

        var contextUtils = contextFinder.findContext(instance.getClass());
        var context = contextUtils.getContext();

        for (var method : methods)
        {
            var annotation = reflectionsUtils.annotations()
                    .findAnnotation()
                    .ofType(BeanProcessor.class)
                    .in(method)
                    .get()
            ;

            if (annotation != null)
            {
                var settings = BeanProcessorsHandler.Settings.getSettings(context);
                var beanPreProcessors = settings.getBeanPreProcessors();

                beanPreProcessors.add((bean) -> {
                    reflectionsUtils.methods()
                            .invoke(instance, method, bean)
                            .orHandle(exceptionsHandler)
                    ;
                });
            }
        }
    }

    public ContextFinder getContextFinder()
    {
        return contextFinder.apply();
    }

    public ExceptionsHandler getExceptionsHandler()
    {
        return exceptionsHandler.apply();
    }

    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
