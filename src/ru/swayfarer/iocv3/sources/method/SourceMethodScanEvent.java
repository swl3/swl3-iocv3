package ru.swayfarer.iocv3.sources.method;

import lombok.var;
import java.lang.reflect.Method;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.sources.SourceScanEvent;

@Data
@Accessors(chain = true)
public class SourceMethodScanEvent {
    
    @NonNull
    public SourceScanEvent sourceScanEvent;
    
    @NonNull
    public ContextBean bean;
    
    @NonNull
    public Method method;
    
    @NonNull
    public Object instance;
}
