package ru.swayfarer.iocv3.sources.method;

import lombok.var;
import ru.swayfarer.iocv3.annotation.Bean;
import ru.swayfarer.iocv3.annotation.LazyBean;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

public class StandardSourceMethodsFuns
{

    public static IFunction1NoR<SourceMethodScanEvent> lazyBean(ReflectionsUtils reflectionsUtils)
    {
        return (event) -> {
            var method = event.getMethod();
            
            var isBeanLazy = reflectionsUtils.annotations().findProperty()
                    .name("value")
                    .marker(LazyBean.class)
                    .type(boolean.class)
                    .in(method)
                    .get()
            ;
            
            if (isBeanLazy != null)
            {
                var bean = event.getBean();
                bean.getInfo().setLazy(isBeanLazy);
            }  
        };
    }
    
    public static void singletonBean(SourceMethodScanEvent event) 
    {
        var bean = event.getBean();
        
        String beanType = bean.getInfo().getBeanType();
        
        if (Bean.Singleton.equals(beanType)) 
        {
            var newValueFun = bean.getValueFun().memorized();
            bean.setValueFun(newValueFun);
        }
    }
    
    public static void threadLocalBean(SourceMethodScanEvent event) 
    {
        var bean = event.getBean();
        
        String beanType = bean.getInfo().getBeanType();
        
        if (Bean.ThreadLocal.equals(beanType)) 
        {
            var newValueFun = bean.getValueFun().threadLocal();
            bean.setValueFun(newValueFun);
        }
    }
}
