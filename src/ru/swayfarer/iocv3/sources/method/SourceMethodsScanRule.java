package ru.swayfarer.iocv3.sources.method;

import lombok.var;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.Bean;
import ru.swayfarer.iocv3.annotation.BeanPriority;
import ru.swayfarer.iocv3.annotation.CanFindByType;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.finder.BeanFinder;
import ru.swayfarer.iocv3.bean.tags.IBeanTags;
import ru.swayfarer.iocv3.sources.SourceScanEvent;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.lang.reflect.InvocationTargetException;

@Getter
@Setter
@Accessors(chain = true)
public class SourceMethodsScanRule implements IFunction1NoR<SourceScanEvent> {
    
    public IObservable<SourceMethodScanEvent> eventScan = Observables.createObservable();
    
    @NonNull
    public IFunction0<BeanFinder> beanFinder;

    @NonNull
    public ExceptionsHandler exceptionsHandler;

    @NonNull
    public ReflectionsUtils reflectionsUtils;

    public SourceMethodsScanRule(@NonNull IFunction0<BeanFinder> beanFinder, @NonNull ExceptionsHandler exceptionsHandler, @NonNull ReflectionsUtils reflectionsUtils)
    {
        this.beanFinder = beanFinder;
        this.exceptionsHandler = exceptionsHandler;
        this.reflectionsUtils = reflectionsUtils;

        eventScan.subscribe().by(StandardSourceMethodsFuns::singletonBean);
        eventScan.subscribe().by(StandardSourceMethodsFuns::threadLocalBean);
        eventScan.subscribe().by(StandardSourceMethodsFuns.lazyBean(reflectionsUtils));
    }

    @Override
    public void applyNoRUnsafe(SourceScanEvent event)
    {
        var instance = event.getInstance();
        var sourceScanner = event.getSourceScanner();
        var contextFinder = sourceScanner.getContextFinder();
        var contextHelper = sourceScanner.getContextHelper().apply();
        
        var beanFinder = getBeanFinder();
        var beanNameFinder = beanFinder.getNameFinder();
        var beanTypeFinder = beanFinder.getTypeFinder();

        var methodFilters = reflectionsUtils.methods().filters();

        var methods = reflectionsUtils.methods().stream(instance)
                .not().filter(methodFilters.mod().statics())
                .filter(methodFilters.annotation().assignable(Bean.class))
                .filter(methodFilters.type().assignables(Object.class))
                .toExList()
        ;
        
        for (var method : methods)
        {
            var contextUtils = contextFinder.findContext(instance.getClass(), method, instance);
            var context = contextUtils.getContext();
            
            try
            {
                var contextBean = new ContextBean(() -> exceptionsHandler.safeReturn(
                        () -> {
                            var foundArgs = beanFinder.findArgs(instance, method, instance.getClass());
                            try
                            {
                                var ret = method.invoke(
                                        instance, 
                                        foundArgs.toArray()
                                );
                                
                                return ret;
                            }
                            catch (Throwable e)
                            {
                                if (e instanceof InvocationTargetException)
                                {
                                    throw e.getCause();
                                }
                                else
                                {
                                    throw e;
                                }
                            }
                        }, 
                        null,
                        "Error while invoking source method", method, "of", instance)
                );
                
                String beanType = reflectionsUtils.annotations().findProperty()
                        .name("type")
                        .marker(Bean.class)
                        .in(method)
                        .type(String.class)
                .get();

                var priority = reflectionsUtils.annotations().findProperty()
                        .name("value")
                        .type(int.class)
                        .marker(BeanPriority.class)
                        .in(method)
                        .orElse(context.getContextSettings().getDefaultBeanPriority())
                ;

                var canFindByType = reflectionsUtils.annotations().findProperty()
                		.name("value")
                		.marker(CanFindByType.class)
                		.in(method)
                		.type(boolean.class)
                		.optional()
                		.orElse(false)
        		;
                
                var beanInfo = contextBean.getInfo();

                beanInfo
                        .setBeanType(beanType)
                        .setCanFindByType(canFindByType)
                        .setPriority(priority)
                        .setNames(beanNameFinder.findNames(instance.getClass(), method, instance))
                ;
                
                beanInfo.setAccociatedType(beanTypeFinder.findBeanType(instance.getClass(), method, instance));
                
                var foundTags = contextHelper.getBeanFinder()
                        .getTagsFinder()
                        .findTags(instance.getClass(), method, instance)
                ;
                
                var scanEvent = new SourceMethodScanEvent(
                        event, 
                        contextBean,
                        method,
                        instance
                );
                
                contextBean.getInfo()
                    .data(IBeanTags.StandartBean)
                    .data(IBeanTags.FromSourceObject)
                    .data(foundTags)
                ;
                
                eventScan.next(scanEvent);

                context.addBean(contextBean);
            }
            catch (Throwable e)
            {
                exceptionsHandler.handle(e, "Error while scanning source method", method, "of", instance);
            }
        }
    }
    
    public BeanFinder getBeanFinder()
    {
        return beanFinder.apply();
    }
}
