package ru.swayfarer.iocv3.sources.method;

import lombok.var;
import java.util.Collection;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.ContextSettingsEditor;
import ru.swayfarer.iocv3.context.Context;
import ru.swayfarer.iocv3.context.registry.ContextRegistry;
import ru.swayfarer.iocv3.context.settings.ContextSettings;
import ru.swayfarer.iocv3.sources.SourceScanEvent;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.ExpressionsList;

@Data
@Accessors(chain = true)
public class ContextSettingsScanRule implements IFunction1NoR<SourceScanEvent>{

    @NonNull
    public ExceptionsHandler exceptionsHandler;

    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Override
    public void applyNoRUnsafe(SourceScanEvent event)
    {
        var sourceClass = event.getInstance().getClass();
        var contextHelper = event.getContextHelper();
        var contextRegistry = contextHelper.getContextRegistry();

        var currentContext = event.getContext();

        var methodFilters = reflectionsUtils.methods().filters();

        var settingsMethods = reflectionsUtils.methods().stream(sourceClass)
            .filter(methodFilters.annotation().assignable(ContextSettingsEditor.class))
            .filter(methodFilters.type().equal(void.class))
            .filter(methodFilters.params().canAcceptTypes(ContextSettings.class))
            .toExList()
        ;
        
        Collection<ContextUtils> values = contextHelper.getContextRegistry().getRegisteredContexts().values();
        
        var allContexts = ExtendedStream.of(values)
                .map(ContextUtils::getContext)
                .toExList()
        ;
        
        for (var method : settingsMethods)
        {
            try
            {
                var contextExpressions = reflectionsUtils.annotations().findProperty()
                        .type(String[].class)
                        .name("value")
                        .marker(ContextSettingsEditor.class)
                        .in(method)
                        .get()
                ;
                
                var expressions = new ExpressionsList(contextExpressions);
                var currentContextName = currentContext.getContextInfo().getName();
                
                expressions.replace(".", currentContextName);
                
                if (expressions.isEmpty())
                    expressions.add(currentContextName);
                
                allContexts
                    .filter((e) -> expressions.isMatches(e.getContextInfo().getName()))
                ;
                
                IFunction1NoR<Context> applyToContextFun = (c) -> {
                    for (var contextUtils : allContexts)
                    {
                        try
                        {
                            method.invoke(event.getInstance(), contextUtils.getContextSettings());
                        }
                        catch (Throwable e)
                        {
                            exceptionsHandler.handle(e, "Error while processing settings method", method, "with context", contextUtils.getContextInfo().getName());
                        }
                    }
                };
                
                // Цепляем на создание новых контекстов, чтобы работало на всех
                contextRegistry.eventContextCreation.subscribe().by(
                        (ctx) -> applyToContextFun.apply(ctx.getContext())
                );
                
                for (var context : allContexts)
                {
                    applyToContextFun.apply(context);
                }
            }
            catch (Throwable e)
            {
                exceptionsHandler.handle(e, "Error while processing settings method", method);
            }
        }
    }
}
