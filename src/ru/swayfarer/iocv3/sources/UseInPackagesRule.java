package ru.swayfarer.iocv3.sources;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.UseInPackages;
import ru.swayfarer.iocv3.annotation.UseContextInThisPackage;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class UseInPackagesRule implements IFunction1NoR<SourceScanEvent>{
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @Override
    public void applyNoRUnsafe(SourceScanEvent event)
    {
        var contextHelper = event.getContextHelper();
        var contextFinder = contextHelper.getContextFinder();
        var reflectionsUtils = getReflectionsUtils();
        var sourceClass = event.getInstance().getClass();
        
        var useInThisPackageAnnotation = reflectionsUtils.annotations().findAnnotation()
            .in(event.getInstance())
            .ofType(UseContextInThisPackage.class)
            .get()
        ;
        
        var contextName = contextFinder.findContext(sourceClass).getContext().getContextInfo().getName();
        var packagesToUse = new ExtendedList<String>();
        
        if (useInThisPackageAnnotation != null)
        {
            packagesToUse.add(sourceClass.getPackage().getName());
        }
        
        var customPackages = reflectionsUtils.annotations().findProperty()
                .marker(UseInPackages.class)
                .type(String[].class)
                .name("value")
                .in(sourceClass)
                .get()
        ;
        
        if (customPackages != null)
        {
            packagesToUse.addAll(customPackages);
        }
        
        if (!CollectionsSWL.isNullOrEmpty(packagesToUse))
        {
            contextHelper.useContextInPackages(contextName, packagesToUse.toArray(String.class));
        }
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
