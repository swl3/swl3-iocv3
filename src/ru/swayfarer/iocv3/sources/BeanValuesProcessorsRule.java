package ru.swayfarer.iocv3.sources;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.BeanValuesProcessor;
import ru.swayfarer.iocv3.annotation.BeanValuesProcessor.EnumBeanState;
import ru.swayfarer.iocv3.context.beanhandlers.BeanValueProcessorsHandler;
import ru.swayfarer.iocv3.context.finder.ContextFinder;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class BeanValuesProcessorsRule implements IFunction1NoR<SourceScanEvent> {

    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @NonNull
    public IFunction0<ContextFinder> contextFinder;
    
    @NonNull
    public IFunction0<ExceptionsHandler> exceptionsHandler;
    
    @Override
    public void applyNoRUnsafe(SourceScanEvent event)
    {
        var instance = event.getInstance();
        var reflectionsUtils = getReflectionsUtils();
        var contextFinder = getContextFinder();
        var exceptionsHandler = getExceptionsHandler();

        var methodFilters = reflectionsUtils.methods().filters();

        var methods = reflectionsUtils.methods().stream(instance)
            .filter(methodFilters.annotation().assignable(BeanValuesProcessor.class))
            .not().filter(methodFilters.mod().statics())
            .not().filter(methodFilters.mod().abstracts())
            .filter(methodFilters.params().count().equal(1))
            .toExList()
        ;
        
        var contextUtils = contextFinder.findContext(instance.getClass());
        var context = contextUtils.getContext();
        
        for (var method : methods)
        {
            var state = reflectionsUtils.annotations().findProperty()
                    .type(EnumBeanState.class)
                    .name("state")
                    .marker(BeanValuesProcessor.class)
                    .in(method)
                    .get()
            ;

            var settings = BeanValueProcessorsHandler.Settings.getSettings(context);
            var beanProcessors = state == EnumBeanState.PostInit ? settings.getPostProcessors() : settings.getPreProcessors();
            beanProcessors.add((o) -> exceptionsHandler.safeReturn(
                    () -> {
                        if (o != null)
                        {
                            var oType = o.getClass();
                            var firstParamType = method.getParameters()[0].getType();
                            
                            var methodReturnType = method.getReturnType();
                            var methodIsVoid = methodReturnType.equals(void.class);
                            
                            if (firstParamType.isAssignableFrom(oType) && (methodIsVoid || firstParamType.isAssignableFrom(methodReturnType)))
                            {
                                var ret = method.invoke(instance, o);
                                
                                // Для void-методов возвращаем старый объект
                                if (methodIsVoid)
                                {
                                   ret = o;
                                }
                                
                                return ret;
                            }
                        }
                        
                        return o;
                    },
                    null,
                    "Error while invoking bean processor method", method, "in", instance)
            );
        }
    }
    
    public ContextFinder getContextFinder()
    {
        return contextFinder.apply();
    }
    
    public ExceptionsHandler getExceptionsHandler()
    {
        return exceptionsHandler.apply();
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }

}
