package ru.swayfarer.iocv3.context.settings;

import lombok.var;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ContextSettings {
    
    public boolean circularDependenciesEnabled = true;
    
    public boolean beansLazyByDefault = true;
    
    public boolean beansRequiredByDefault = true;

    public int defaultBeanPriority = 0;
}
