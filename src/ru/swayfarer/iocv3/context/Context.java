package ru.swayfarer.iocv3.context;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.tags.IBeanTags.BeanExceptions;
import ru.swayfarer.iocv3.context.settings.ContextSettings;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;

import java.util.concurrent.atomic.AtomicBoolean;

@Data
@Accessors(chain = true)
public class Context {
    
    @NonNull
    public AtomicBoolean isDestroyed = new AtomicBoolean();

    @NonNull
    public IObservable<ContextBean> eventAddBean = Observables.createObservable()
            .exceptions().configure((eh) -> {
                eh.configure().rule()
                    .any()
                    .handle((event) -> {
                        var tags = event.getTags();
                        tags.add(BeanExceptions.tagAddBean);
                        event.getCustomAttributes().put(BeanExceptions.keyTargetContext, this);
                    })
                ;
            })
    ;
    
    @NonNull
    public ContextInfo contextInfo = new ContextInfo();
    
    @NonNull
    public ContextSettings contextSettings = new ContextSettings();
    
    @NonNull
    public ExtendedList<ContextBean> beans = new ExtendedList<>();

    public ExtendedMap<String, Object> customData = new ExtendedMap<>().concurrent();

    public Context addBean(@NonNull ContextBean contextBean)
    {
        eventAddBean.next(contextBean);
        beans.add(contextBean);

        return this;
    }
    
    public boolean isDestroyed()
    {
        return isDestroyed.get();
    }

    
    public static class Priority {

        public static int next = 0;

        public static int compoundBeans = (next += 200);

        public static int beanProcessorsPriority = (next += 200);

        public static int preProcessValuesPriority = (next += 200);
        
        public static int postProcessValuesPriority = (next += 200);

        public static int lazyPriority = (next += 200);

        public static int preTypesPriority = (next += 200);

        public static int injectorsPriority = (next += 200);

        public static int initializersPriority = (next += 200);

        public static int injectInstancePriority = (next += 200);

        public static int postTypesPriority = (next += 200);

        public static int completeBeans = (next += 200);
    }
}
