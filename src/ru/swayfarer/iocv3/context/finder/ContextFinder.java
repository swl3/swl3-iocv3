package ru.swayfarer.iocv3.context.finder;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.finder.ClassEventTarget;
import ru.swayfarer.iocv3.bean.finder.FieldEventTarget;
import ru.swayfarer.iocv3.bean.finder.MethodEventTarget;
import ru.swayfarer.iocv3.context.finder.event.ContextByClassEvent;
import ru.swayfarer.iocv3.context.finder.event.ContextByFieldEvent;
import ru.swayfarer.iocv3.context.finder.event.ContextByMethodEvent;
import ru.swayfarer.iocv3.context.finder.event.ContextByMethodParamEvent;
import ru.swayfarer.iocv3.context.registry.ContextRegistry;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

@Data
@Accessors(chain = true)
public class ContextFinder {

    @NonNull
    public FunctionsChain1<ContextByClassEvent, ContextUtils> findByClassFuns = new FunctionsChain1<>();
    
    @NonNull
    public FunctionsChain1<ContextByFieldEvent, ContextUtils> findByFieldFuns = new FunctionsChain1<>();
    
    @NonNull
    public FunctionsChain1<ContextByMethodEvent, ContextUtils> findByMethodFuns = new FunctionsChain1<>();
    
    @NonNull
    public FunctionsChain1<ContextByMethodParamEvent, ContextUtils> findByMethodParamFuns = new FunctionsChain1<>();
    
    @NonNull
    public IFunction0<ContextRegistry> contextRegistry;

    public ContextUtils findContext(@NonNull Class<?> cl)
    {
        return findContext(
                (ContextByClassEvent) new ContextByClassEvent()
                    .setTarget(new ClassEventTarget(cl))
        );
    }
    
    public ContextUtils findContext(@NonNull ContextByMethodParamEvent event)
    {
        event.setContextFinder(this);
        return orDefault(findByMethodParamFuns.apply(event));
    }
    
    public ContextUtils findContext(@NonNull ContextByClassEvent event)
    {
        event.setContextFinder(this);
        return orDefault(findByClassFuns.apply(event));
    }
    
    public ContextUtils findContext(@NonNull ContextByFieldEvent event)
    {
        event.setContextFinder(this);
        return orDefault(findByFieldFuns.apply(event));
    }

    public ContextUtils findContext(@NonNull Class<?> classWithField, Field field, Object instance)
    {
        var event = new ContextByFieldEvent();

        var methodEventTarget = new FieldEventTarget(classWithField, field);

        methodEventTarget
                .setInstanceWithField(instance)
        ;

        event.setTarget(methodEventTarget);

        return findContext(event);
    }
    
    public ContextUtils findContext(@NonNull Class<?> classWithMethod, Method method, Object instance)
    {
        var event = new ContextByMethodEvent();

        var methodEventTarget = MethodEventTarget.readMethod(method, instance);
        methodEventTarget.setClassWithMethod(classWithMethod);

        event.setTarget(methodEventTarget);
        
        return findContext(event);
    }
    
    public ContextUtils findContext(@NonNull ContextByMethodEvent event)
    {
        event.setContextFinder(this);
        return orDefault(findByMethodFuns.apply(event));
    }
    
    public ContextUtils orDefault(ContextUtils utils)
    {
    	return utils == null ? findDefaultContext() : utils;
    }

    public ContextUtils findDefaultContext()
    {
    	return getContextRegistry().apply().findOrCreateContext("default");
    }
    
}
