package ru.swayfarer.iocv3.context.finder.event;

import lombok.var;
import ru.swayfarer.iocv3.bean.finder.FieldEventTarget;
import ru.swayfarer.iocv3.context.finder.FindContextEvent;

public class ContextByFieldEvent extends FindContextEvent<FieldEventTarget> {}
