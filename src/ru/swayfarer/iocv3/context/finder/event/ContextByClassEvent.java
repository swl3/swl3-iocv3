package ru.swayfarer.iocv3.context.finder.event;

import lombok.var;
import ru.swayfarer.iocv3.bean.finder.ClassEventTarget;
import ru.swayfarer.iocv3.context.finder.FindContextEvent;

public class ContextByClassEvent extends FindContextEvent<ClassEventTarget> {}
