package ru.swayfarer.iocv3.context.finder.event;

import lombok.var;
import ru.swayfarer.iocv3.bean.finder.MethodEventTarget;
import ru.swayfarer.iocv3.context.finder.FindContextEvent;

public class ContextByMethodEvent extends FindContextEvent<MethodEventTarget>{}
