package ru.swayfarer.iocv3.context.finder.event;

import lombok.var;
import ru.swayfarer.iocv3.bean.finder.ParameterEventTarget;
import ru.swayfarer.iocv3.context.finder.FindContextEvent;

public class ContextByMethodParamEvent extends FindContextEvent<ParameterEventTarget> {}
