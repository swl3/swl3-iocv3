package ru.swayfarer.iocv3.context.finder.rules;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.context.finder.event.ContextByMethodEvent;
import ru.swayfarer.iocv3.context.finder.event.ContextByMethodParamEvent;
import ru.swayfarer.iocv3.context.registry.ContextRegistry;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class FindContextByParamRule implements IFunction1<ContextByMethodParamEvent, ContextUtils> {

    @NonNull
    public IFunction0<ContextRegistry> contextRegistry;
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    public FindContextByAnnotationRule annotationsRule = new FindContextByAnnotationRule(
            this::getContextRegistry, 
            this::getReflectionsUtils
    );
    
    @Override
    public ContextUtils applyUnsafe(ContextByMethodParamEvent event)
    {
        var target = event.getTarget();
        var param = target.getParameter();
        var anntotations = param.getAnnotations();
        
        var ret = annotationsRule.apply(anntotations);
        
        if (ret != null)
            return ret;
        
        ret = event.getContextFinder().findContext(
                (ContextByMethodEvent) new ContextByMethodEvent()
                    .setTarget(target.getMethod())
        );
        
        if (ret != null)
            return ret;
        
        return ret;
    }
    
    public ContextRegistry getContextRegistry()
    {
        return contextRegistry.apply();
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
