package ru.swayfarer.iocv3.context.finder.rules;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.context.finder.event.ContextByClassEvent;
import ru.swayfarer.iocv3.context.registry.ContextRegistry;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class FindContextByClassRule implements IFunction1<ContextByClassEvent, ContextUtils> {

    @NonNull
    public IFunction0<ContextRegistry> contextRegistry;
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    public FindContextByAnnotationRule annotationsRule = new FindContextByAnnotationRule(
            this::getContextRegistry, 
            this::getReflectionsUtils
    );
    
    @Override
    public ContextUtils applyUnsafe(ContextByClassEvent event)
    {
        var targetClass = event.getTarget().getType();
        
        var ret = annotationsRule.apply(targetClass.getAnnotations());
        return ret;
    }
    
    public ContextRegistry getContextRegistry()
    {
        return contextRegistry.apply();
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
