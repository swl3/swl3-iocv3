package ru.swayfarer.iocv3.context.finder.rules;

import lombok.var;
import java.lang.annotation.Annotation;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.ContextName;
import ru.swayfarer.iocv3.context.registry.ContextRegistry;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class FindContextByAnnotationRule {

    @NonNull
    public IFunction0<ContextRegistry> contextRegistry;
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    public ContextUtils apply(Annotation[] annotations)
    {
        var reflectionsUtils = getReflectionsUtils();
        
        var foundContextName = reflectionsUtils.annotations().findProperty()
            .name("value")
            .marker(ContextName.class)
            .type(String.class)
            .in(annotations)
            .get()
        ;
        
        if (foundContextName != null)
        {
            var contextRegistry = getContextRegistry();
            
            if (contextRegistry != null)
            {
                return contextRegistry.findOrCreateContext(foundContextName);
            }
        }

        return null;
    }
    
    public ContextRegistry getContextRegistry()
    {
        return contextRegistry.apply();
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }       
}
