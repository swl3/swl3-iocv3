package ru.swayfarer.iocv3.context.finder.rules;

import lombok.var;
import java.lang.annotation.Annotation;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.context.finder.event.ContextByMethodEvent;
import ru.swayfarer.iocv3.context.registry.ContextRegistry;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class FindContextByMethodRule implements IFunction1<ContextByMethodEvent, ContextUtils> {

    @NonNull
    public IFunction0<ContextRegistry> contextRegistry;
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    public FindContextByAnnotationRule annotationsRule = new FindContextByAnnotationRule(
            this::getContextRegistry, 
            this::getReflectionsUtils
    );
    
    @Override
    public ContextUtils applyUnsafe(ContextByMethodEvent event)
    {
        var method = event.getTarget().getAnnotations();
        var ret = annotationsRule.apply(method.toArray(Annotation.class));
        
        if (ret != null)
            return ret;
        
        var methodClass = event.getTarget().getClassWithMethod();
        
        ret = event.getContextFinder().findContext(methodClass);
        
        if (ret != null)
            return ret;
        
        return ret;
    }
    
    public ContextRegistry getContextRegistry()
    {
        return contextRegistry.apply();
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
