package ru.swayfarer.iocv3.context.finder.rules;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.context.finder.event.ContextByFieldEvent;
import ru.swayfarer.iocv3.context.registry.ContextRegistry;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class FieldAnnotationsRule implements IFunction1<ContextByFieldEvent, ContextUtils>{

    @NonNull
    public IFunction0<ContextRegistry> contextRegistry;
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    public FindContextByAnnotationRule annotationsRule = new FindContextByAnnotationRule(
            this::getContextRegistry, 
            this::getReflectionsUtils
    );
    
    @Override
    public ContextUtils applyUnsafe(ContextByFieldEvent event)
    {
        var target = event.getTarget();
        var field = target.getField();
        var ret = annotationsRule.apply(field.getAnnotations());
        
        if (ret != null)
            return ret;
        
        ret = event.getContextFinder().findContext(target.getClassWithField());
        
        if (ret != null)
            return ret;
        
        return ret;
    }
    
    public ContextRegistry getContextRegistry()
    {
        return contextRegistry.apply();
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
