package ru.swayfarer.iocv3.context.finder.rules;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.context.finder.event.ContextByClassEvent;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.ExpressionsList;

@Data
@Accessors(chain = true)
public class ContextPatternRule implements IFunction1<ContextByClassEvent, ContextUtils> {
    
    public ExpressionsList expressionsList = new ExpressionsList();
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @NonNull
    public IFunction0<ContextUtils> contextUtils;
    
    @Override
    public ContextUtils applyUnsafe(ContextByClassEvent event)
    {
        var cl = event.getTarget().getType();
        
        if (cl != null)
        {
            if (expressionsList.isMatches(cl.getName()))
            {
                return getContextUtils();
            }
        }
        
        return null;
    }
    
    public ContextUtils getContextUtils()
    {
        return contextUtils.apply();
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
