package ru.swayfarer.iocv3.context.finder;

import lombok.var;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class FindContextEvent<Target_Type> {
    public ContextFinder contextFinder;
    public Target_Type target;
}
