package ru.swayfarer.iocv3.context.registry;

import lombok.var;
import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.context.Context;
import ru.swayfarer.iocv3.context.registry.handlers.StandartContextHandlers;
import ru.swayfarer.iocv3.injection.ContextInjector;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.swconf2.config.properties.ConfigPropertiesHelper;

@Data
@Accessors(chain = true)
public class ContextRegistry {
    
    public Map<String, ContextUtils> registeredContexts = new HashMap<>();
    
    @NonNull
    public IFunction0<ExceptionsHandler> exceptionsHandler;
    
    @NonNull
    public IFunction0<ContextInjector> contextInjector;
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @NonNull
    public IObservable<ContextUtils> eventContextCreation = Observables.createObservable();

    public ContextUtils getDefaultContext()
    {
        return findOrCreateContext("default");
    }

    public ContextUtils findOrCreateContext(@NonNull String name)
    {
        var ret = registeredContexts.get(name);
        
        if (ret == null)
        {
            var context = new Context();
            
            context.getContextInfo()
                .setName(name)
            ;
            
            ret = new ContextUtils(this::getReflectionsUtils, this::getExceptionsHandler, context);
            
            eventContextCreation.next(ret);
            
            registeredContexts.put(name, ret);
        }
        
        return ret;
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
    
    public ExceptionsHandler getExceptionsHandler()
    {
        return exceptionsHandler.apply();
    }
    
    public ContextInjector getContextInjector()
    {
        return contextInjector.apply();
    }

    public static class Priority {
        public static int swconfPropertiesInitPriority = 100;
        public static int injectBeansPriority = 200;
        public static int initBeansPriority = 300;
    }
}
