package ru.swayfarer.iocv3.context.registry.handlers;

import ru.swayfarer.iocv3.context.beanhandlers.Swconf2BeanHandler;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.swconf2.config.properties.ConfigPropertiesHelper;

public class StandartContextHandlers {

    public static IFunction1NoR<ContextUtils> initSwconfProperties(IFunction0<ReflectionsUtils> reflectionsUtils, ConfigPropertiesHelper propertiesHelper)
    {
        return (contextUtils) -> {
            contextUtils.getContext().eventAddBean.subscribe().by(new Swconf2BeanHandler(
                    () -> RLUtils.defaultUtils(), 
                    reflectionsUtils, 
                    propertiesHelper, 
                    contextUtils.getContext()
            ));
        };
    }
}
