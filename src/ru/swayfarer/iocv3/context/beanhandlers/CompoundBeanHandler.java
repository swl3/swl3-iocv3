package ru.swayfarer.iocv3.context.beanhandlers;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.iocv3.bean.BeanType;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.finder.BeanFindingEvent;
import ru.swayfarer.iocv3.componentscan.ComponentScan;
import ru.swayfarer.iocv3.exception.BeanInitializationException;
import ru.swayfarer.iocv3.helper.ContextHelper;
import ru.swayfarer.swl3.asmv2.proxy.CompoundProxyInvocationHandler;
import ru.swayfarer.swl3.asmv2.proxy.ReflectionEndProxyConfiguration;
import ru.swayfarer.swl3.asmv2.proxy.ReflectionProxyHelper;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.lang.reflect.Modifier;

@Getter
@Setter
@Accessors(chain = true)
public class CompoundBeanHandler implements GeneratedFuns.IFunction1NoR<ContextBean>
{
    @Internal
    public AsmUtilsV2 asmUtils;

    @Internal
    public ContextHelper contextHelper;

    @Internal
    public ReflectionsUtils reflectionsUtils;

    public static final String compoundBeanTag = "#compoundbean";

    public CompoundBeanHandler(AsmUtilsV2 asmUtils, ContextHelper contextHelper, ReflectionsUtils reflectionsUtils)
    {
        this.asmUtils = asmUtils;
        this.contextHelper = contextHelper;
        this.reflectionsUtils = reflectionsUtils;
    }

    @Override
    public void applyNoRUnsafe(ContextBean bean) throws Throwable
    {
        ComponentScan.addLazyBeanCreationClassLoading(bean, () -> {
            var beanCustomData = bean.getInfo().getCustomData();

            if (beanCustomData.has(compoundBeanTag))
            {
                if (ComponentScan.isBeanFromComponentScan(bean))
                {
                    var oldCreationClass = ComponentScan.getBeanCreationClass(bean);
                    var allInterfaces = reflectionsUtils.interfaces().stream(oldCreationClass).toExList();

                    var compoundHandler = new CompoundProxyInvocationHandler(asmUtils);

                    allInterfaces.exStream().distinct().each((i) -> {
                        var context = contextHelper.getContextFinder().findContext(oldCreationClass);
                        var findingEvent = new BeanFindingEvent(context.getContext());
                        findingEvent.setBeanRequired(true);
                        findingEvent.setInjectionTarget("compound class creation: " + oldCreationClass.getName());
                        findingEvent.getTargetBeanInfo()
                                .setAccociatedType(new BeanType().setClassType(i))
                                .setCanFindByType(true)
                        ;

                        findingEvent.getExcludes().add(bean);

                        try
                        {
                            var foundBean = contextHelper.getBeanFinder().findBean(findingEvent);

                            if (foundBean != null)
                            {
                                compoundHandler.registerPart(i, foundBean::getValue);
                            }
                        }
                        catch (Throwable e)
                        {
                            ExceptionsUtils.throwException(new BeanInitializationException("Can't create compound bean for class " + oldCreationClass + ", typed bean for interface " + i.getName() + " was not found!", e));
                        }
                    });

                    var proxyHelper = new ReflectionProxyHelper(asmUtils, reflectionsUtils);

                    var proxyClass = proxyHelper.newProxyClass(
                            compoundHandler,
                            (cfg) -> {
                                compoundHandler.methodKeyToInstanceFun.keySet()
                                        .stream()
                                        .filter((partAssocClass) -> Modifier.isInterface(partAssocClass.getModifiers()))
                                        .forEach(cfg::addInterface)
                                ;

                                cfg.setHandlerFieldStatic(true);
                                cfg.setParentType(oldCreationClass);
                            }
                    );

                    return proxyClass;
                }
                else
                {
                    ExceptionsUtils.throwException(new IllegalStateException("Can't create compound bean from non-component-scan source!"));
                }
            }

            return null;
        });
    }
}
