package ru.swayfarer.iocv3.context.beanhandlers;

import lombok.var;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.context.Context;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

import java.util.UUID;

@Data
@Accessors(chain = true)
public class BeanValueProcessorsHandler implements IFunction1NoR<ContextBean> {

    @NonNull
    public Context context;

    @NonNull
    public IFunction1<Settings, ExtendedList<IFunction1<Object, Object>>> fun;
    
    @Override
    public void applyNoRUnsafe(ContextBean bean)
    {
        var beanProcessors = getPostProcessors();
        var valueFun = bean.getValueFun();

        if (!CollectionsSWL.isNullOrEmpty(beanProcessors))
        {
            for (var beanProcessor : beanProcessors)
            {
                valueFun = valueFun.andApply(beanProcessor);
            }
            
            bean.setValueFun(valueFun);
        }
    }
    
    public ExtendedList<IFunction1<Object, Object>> getPostProcessors()
    {
        return fun.apply(Settings.getSettings(context));
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    public static class Settings {

        public static String settingsKey = UUID.randomUUID().toString();

        @NonNull
        public ExtendedList<IFunction1<Object, Object>> preProcessors = CollectionsSWL.list();

        @NonNull
        public ExtendedList<IFunction1<Object, Object>> postProcessors = CollectionsSWL.list();

        public static Settings getSettings(Context context)
        {
            return context.getCustomData().getOrCreate(settingsKey, Settings::new);
        }
    }
}
