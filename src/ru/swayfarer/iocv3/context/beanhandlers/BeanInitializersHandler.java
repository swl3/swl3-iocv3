package ru.swayfarer.iocv3.context.beanhandlers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.OnBeanInit;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.thread.ThreadsUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;

@Data
@Accessors(chain = true)
public class BeanInitializersHandler implements IFunction1NoR<ContextBean> {

    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @NonNull
    public IFunction0<ExceptionsHandler> exceptionsHandler;

    @Internal
    public IFunction0<ExtendedMap<Class<?>, ExtendedList<InitMethodEntry>>> initMethodsCache = ThreadsUtils.singleton(ExtendedMap::new);

    public OnceBeanHelper onceBeanHelper = new OnceBeanHelper();
    
    @Override
    public void applyNoRUnsafe(ContextBean bean)
    {
        var reflectionsUtils = getReflectionsUtils();
        var exceptionsHandler = getExceptionsHandler();
        
        var oldCreationFun = bean.getValueFun();
        
        bean.setValueFun(() -> {
            var value = ContextBean.executeValueFun(oldCreationFun, bean);
            
            if (value == null)
                return value;

            if (!onceBeanHelper.containsOrAdd(bean, value))
            {
                BeanHandlingTrace.trace("Initializing bean", bean.getInfo().getNames());

                var initMethods = initMethodsCache.apply().getOrCreate(value.getClass(), () -> {
                    var methodFilters = reflectionsUtils.methods().filters();

                    var methods = reflectionsUtils.methods().stream(value)
                        .filter(methodFilters.annotation().assignable(OnBeanInit.class))
                        .not().filter(methodFilters.mod().abstracts())
                        .not().filter(methodFilters.mod().statics())
                        .filter(methodFilters.params().count().equal(0))
                        .map((method) -> {
                            var tags = reflectionsUtils.annotations().findProperty()
                                    .in(method)
                                    .name("exceptionsTags")
                                    .marker(OnBeanInit.class)
                                    .type(String[].class)
                                    .orElse(new String[0])
                            ;

                            var priority = reflectionsUtils.annotations().findProperty()
                                    .in(method)
                                    .name("priority")
                                    .marker(OnBeanInit.class)
                                    .type(int.class)
                                    .orElse(0)
                            ;

                            return InitMethodEntry.builder()
                                    .tags(CollectionsSWL.list(tags))
                                    .method(method)
                                    .priority(priority)
                                    .build()
                            ;
                        })
                        .sort(Comparator.comparingInt(InitMethodEntry::getPriority))
                        .toExList()
                    ;

                    return ReflectionsUtils.cast(methods);
                });

                for (var initMethodEntry : initMethods)
                {
                    exceptionsHandler.handle()
                            .message("Error while invoking bean initialization handler method", initMethodEntry.getMethod(), "in", value)
                            .tag(initMethodEntry.getTags())
                            .and().unwrap(InvocationTargetException.class)
                            .start(() -> {
                                initMethodEntry.getMethod().invoke(value);
                            })
                    ;
                }
            }

            return value;
        });
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @SuperBuilder
    public static class InitMethodEntry {
        public Method method;
        public int priority;
        public ExtendedList<String> tags = new ExtendedList<>();
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
    
    public ExceptionsHandler getExceptionsHandler()
    {
        return exceptionsHandler.apply();
    }
}
