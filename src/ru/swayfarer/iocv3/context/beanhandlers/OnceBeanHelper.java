package ru.swayfarer.iocv3.context.beanhandlers;

import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.swl3.collections.set.WeakHashSet;
import ru.swayfarer.swl3.markers.Internal;

import java.util.Collections;
import java.util.Set;

public class OnceBeanHelper
{
    public Set<Object> injectionsCache = Collections.synchronizedSet(new WeakHashSet<>());

    public boolean containsOrAdd(ContextBean bean, Object value)
    {
        if (value == null)
            return false;

        if (!hasValue(value))
        {
            injectionsCache.add(value);

            return false;
        }

        return true;
    }

    @Internal
    public boolean hasValue(Object obj)
    {
        return injectionsCache.contains(obj);
    }
}
