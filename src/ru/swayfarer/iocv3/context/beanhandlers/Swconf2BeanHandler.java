package ru.swayfarer.iocv3.context.beanhandlers;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.ConfigurationSwconf2;
import ru.swayfarer.iocv3.bean.BeanType;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.tags.IBeanTags;
import ru.swayfarer.iocv3.context.Context;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.observable.property.ObservableProperty;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.config.Swconf2Config;
import ru.swayfarer.swl3.swconf2.config.properties.ConfigPropertiesHelper;

@Data
@Accessors(chain = true)
public class Swconf2BeanHandler implements IFunction1NoR<ContextBean>{

    public ILogger logger = LogFactory.getLogger();

    public static final String propertyBeanTag = "#swconf2-property";
    
    @NonNull
    public IFunction0<RLUtils> rlUtils;
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @NonNull
    public ConfigPropertiesHelper propertiesHelper;
    
    @NonNull
    public Context context;
    
    @Override
    public void applyNoRUnsafe(ContextBean bean)
    {
        var reflectionsUtils = getReflectionsUtils();
        var beanClassType = bean.getInfo().getAccociatedType().getClassType();

        var configurationAnnotation = reflectionsUtils.annotations().findAnnotation()
                .ofType(ConfigurationSwconf2.class)
                .in(beanClassType)
                .get()
        ;

        if (configurationAnnotation == null)
            return;

        if (Swconf2Config.class.isAssignableFrom(beanClassType))
        {
            var oldValueFun = bean.getValueFun();
            
            IFunction0<Object> newValueFun = () -> {
                var value = (Swconf2Config) oldValueFun.apply();
                
                var prefix = reflectionsUtils.annotations().findProperty()
                    .name("prefix")
                    .type(String.class)
                    .marker(ConfigurationSwconf2.class)
                    .in(value.getClass())
                    .get()
                ;
                
                var location = reflectionsUtils.annotations().findProperty()
                    .name("location")
                    .type(String.class)
                    .marker(ConfigurationSwconf2.class)
                    .in(value.getClass())
                    .get()
                ;
                
                var autoSaveDelay = reflectionsUtils.annotations().findProperty()
                    .name("autoSaveDelay")
                    .type(long.class)
                    .marker(ConfigurationSwconf2.class)
                    .in(value.getClass())
                    .get()
                ;
                
                if (autoSaveDelay != null && autoSaveDelay > 0)
                {
                    value.config()
                        .enableAutoSave(autoSaveDelay)
                    ;
                }
                
                var rlutils = getRlutils();
                
                if (!StringUtils.isBlank(location))
                {
                    value.config()
                        .createIfNotFound()
                        .load(rlutils.createLink(location))
                    ;
                }
                
                var properties = propertiesHelper.getProperties(prefix, value);
                
                for (var entry : properties.properties.entrySet())
                {
                    var accessor = entry.getValue();
                    var propertyBean = new ContextBean(() -> accessor.getValue());
                    
                    propertyBean.getInfo()
                        .data(IBeanTags.GeneratedBean)
                        .data(IBeanTags.FromProperty)
                        .name(entry.getKey())
                        .setAccociatedType(BeanType.of(accessor.getAssociatedType()))
                    ;
                    
                    if (!accessor.getAssociatedType().equals(ObservableProperty.class))
                    {
                        var observableProperty = Observables.createProperty(accessor.getValue());
                        
                        observableProperty.eventPostChange.subscribe().by((e) -> {
                            accessor.setValue(e.getNewValue());
                            value.config().configInfo.setNeedsToSave(true);
                        });
                        
                        var observablePropertyBean = new ContextBean(() -> observableProperty);
                        
                        observablePropertyBean.getInfo()
                            .data(IBeanTags.FromProperty)
                            .data(IBeanTags.GeneratedBean)
                            .name(entry.getKey())
                            .setAccociatedType(BeanType.of(ObservableProperty.class))
                        ;
                        
                        context.addBean(observablePropertyBean);
                    }
                    else
                    {
                        var observable = (ObservableProperty<?>) accessor.getValue();
                        var propertyValue = observable.getValue();
                        
                        if (propertyValue != null)
                        {
                            var classOfPropertyValue = propertyValue.getClass();
                            var propertyValueBean = new ContextBean(() -> propertyValue);
                            
                            propertyValueBean.getInfo()
                                .data(IBeanTags.FromProperty)
                                .data(IBeanTags.GeneratedBean)
                                .name(entry.getKey())
                                .setAccociatedType(BeanType.of(classOfPropertyValue))
                            ;
                            
                            context.addBean(propertyValueBean);
                        }
                    }
                    
                    context.addBean(propertyBean);
                }
                
                return value;
            };
            
            bean.setValueFun(newValueFun);
            bean.getValue();
        }
        else
        {
            logger.warn("Found bean marked by @ConfigurationSwconf, but that's type does not extend Swconf2Config! (", beanClassType, ")");
        }
    }
    
    public RLUtils getRlutils()
    {
        return rlUtils.apply();
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
