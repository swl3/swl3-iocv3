package ru.swayfarer.iocv3.context.beanhandlers;

import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;

public class BeanHandlingTrace
{
    public static boolean enabled;
    public static ILogger logger = LogFactory.getLogger();

    public static void trace(Object... text)
    {
        if (enabled)
            logger.dev(text);
    }
}
