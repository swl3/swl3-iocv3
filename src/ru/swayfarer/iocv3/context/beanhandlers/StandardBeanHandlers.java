package ru.swayfarer.iocv3.context.beanhandlers;

import lombok.var;
import ru.swayfarer.iocv3.annotation.Bean;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.tags.IBeanTags;
import ru.swayfarer.iocv3.exception.CyclicDependenciesException;
import ru.swayfarer.iocv3.injection.IContextInjector;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.thread.ThreadsUtils;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class StandardBeanHandlers
{
    public static void addOnceAction(ContextBean bean, IFunction0NoR fun)
    {
        var prevValueFun = bean.getValueFun();
        var isSetted = new AtomicBoolean();

        bean.setValueFun(() -> {
            if (!isSetted.get())
            {
                synchronized (isSetted)
                {
                    if (!isSetted.get())
                    {
                        isSetted.set(true);
                        fun.apply();
                    }
                }
            }

            return prevValueFun.apply();
        });
    }

    public static void singletonBean(ContextBean bean)
    {
        String beanType = bean.getInfo().getBeanType();

        if (Bean.Singleton.equals(beanType))
        {
            bean.getInfo().getCustomData().add(IBeanTags.CanBeSkipedAtCircularDependency);
            var newValueFun = bean.getValueFun().memorized();
            bean.setValueFun(newValueFun);
        }
    }

    public static void threadLocalBean(ContextBean bean)
    {
        String beanType = bean.getInfo().getBeanType();

        if (Bean.ThreadLocal.equals(beanType))
        {
            bean.getInfo().getCustomData().add(IBeanTags.CanBeSkipedAtCircularDependency);
            var newValueFun = bean.getValueFun().threadLocal();
            bean.setValueFun(newValueFun);
        }
    }

    public static IFunction1NoR<ContextBean> injectBeans(ContextUtils contextUtils, IContextInjector injector)
    {
        // Вложенность инъекции на текущий момент.
        IFunction0<AtomicLong> injectionsState = ThreadsUtils.threadLocal(() -> new AtomicLong(0));

        // Все бины, в которые в текущей инъекционной сессии уже были внедрены зависимости
        IFunction0<ExtendedList<ContextBean>> alreadyInjectedBeans = ThreadsUtils.threadLocal(ExtendedList::new);

        return (bean) -> {
            var oldValueFun = bean.getValueFun();
            var newValueFun = oldValueFun.andApply((o) -> {

                if (o == null)
                    return o;

                var visitedBeans = alreadyInjectedBeans.apply();
                var isDependencyCircular = visitedBeans.contains(bean);

                // Заменяем функцию получения бина на отдающую тупо это значение на время инъекции
                var oldFun = bean.getValueFun();
                try
                {
                    if (isSkipsCircularDependencies(bean))
                        bean.setValueFun(() -> o);

                    if (!isDependencyCircular)
                    {
                        BeanHandlingTrace.trace("Injecting beans to", bean.getInfo().getNames());
                        visitedBeans.add(bean);

                        var state = injectionsState.apply();

                        state.incrementAndGet();

                        injector.inject(o);

                        state.decrementAndGet();
                        visitedBeans.remove(bean);

                        if (state.get() == 0)
                        {
                            visitedBeans.clear();
                        }
                    }
                    else
                    {
                        boolean cyclicDependendenciesEnabled = contextUtils.getContext().getContextSettings().isCircularDependenciesEnabled();

                        if (!cyclicDependendenciesEnabled)
                        {
                            DynamicString buffer = generateCircularDependencyException("Circular bean dependency is not allowed ", bean, visitedBeans);
                            ExceptionsUtils.throwException(new CyclicDependenciesException(buffer.toString()));
                        }
                        else if (!isSkipsCircularDependencies(bean))
                        {
                            DynamicString buffer = generateCircularDependencyException("Can't skip bean at circular dependency!", bean, visitedBeans);
                            ExceptionsUtils.throwException(new CyclicDependenciesException(buffer.toString()));
                        }
                        else
                        {
                            BeanHandlingTrace.trace("Skipped circular dependency for bean", bean.getInfo().getNames());
                        }
                    }

                    return o;
                }
                finally
                {
                    bean.setValueFun(oldFun);
                }
            });

            bean.setValueFun(newValueFun);
        };
    }

    public static boolean isSkipsCircularDependencies(ContextBean bean)
    {
        return bean.getInfo().getCustomData().has(IBeanTags.CanBeSkipedAtCircularDependency);
    }

    private static DynamicString generateCircularDependencyException(String prefix, ContextBean bean, ExtendedList<ContextBean> visitedBeans)
    {
        var buffer = new DynamicString(prefix);

        boolean isFirst = true;
        boolean showFullClassName = false;
        var visitedClassesList = new ArrayList<>();

        var beansInChain = visitedBeans.copy();

        for (var injectedBean : beansInChain)
        {
            var classType = injectedBean.getInfo().getAccociatedType().getClassType();

            if (visitedClassesList.contains(classType))
            {
                showFullClassName = true;
                break;
            }

            visitedClassesList.add(classType);
        }

        beansInChain.add(bean);

        AtomicLong heres = new AtomicLong(0);

        // Подставляет префикс, но без бина
        IFunction0NoR prefixFun = () -> {
            if (heres.get() > 0)
                buffer.append("|  ");
            else
                buffer.append("   ");
        };

        // Подставляет префикс бину
        IFunction1NoR<ContextBean> beanPrefixFun = (b) -> {
            var isHere = b.equals(bean);

            if (heres.get() > 0)
            {
                if (isHere)
                {
                    buffer.append(" <<");
                }
                else
                {
                    buffer.append("|  ");
                }

                heres.incrementAndGet();
            }
            else if (isHere)
            {
                buffer.append(" >>");
                heres.incrementAndGet();
            }
            else
            {
                buffer.append("   ");
            }
        };

        for (var injectedBean : beansInChain)
        {
            buffer.newLine();

            if (!isFirst)
            {
                prefixFun.apply();

                buffer.append("  |");
                buffer.newLine();

                prefixFun.apply();
                buffer.append("  V");
                buffer.newLine();
            }

            beanPrefixFun.apply(injectedBean);

            isFirst = false;

            buffer.append(injectedBean.getInfo().getNames());
            buffer.append(" of ");
            var classType = injectedBean.getInfo().getAccociatedType().getClassType();

            if (showFullClassName)
                buffer.append(classType.getName());
            else
                buffer.append(classType.getSimpleName());

        }
        return buffer;
    }

    public static void setCacheInjections(ContextBean bean, boolean enable)
    {
        bean.getInfo().getCustomData().add("options/swl3-iocv3-cache-injections", enable);
    }

    public static boolean isCacheInjections(ContextBean bean)
    {
        return Boolean.TRUE.equals(bean.getInfo().getCustomData().get("options/swl3-iocv3-cache-injections"));
    }
}
