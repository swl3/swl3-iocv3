package ru.swayfarer.iocv3.context.beanhandlers;

import lombok.var;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;

public class CompleteBeanValueHandler implements IFunction1NoR<ContextBean>
{
    @Override
    public void applyNoRUnsafe(ContextBean bean) throws Throwable
    {
        var prevValueFun = bean.getValueFun();

        bean.setValueFun(() -> {
            if (hasCompleteBeanEvent(bean))
            {
                getOrCreateCompleteBeanEvent(bean).next(bean);
                BeanHandlingTrace.trace("Completing bean", bean.getInfo().getNames());
            }

            return prevValueFun.apply();
        });
    }

    public static boolean hasCompleteBeanEvent(ContextBean bean)
    {
        return bean.getInfo().getCustomData().getDataMap().containsKey("swl3/ioc/completeBeanEvent");
    }

    public static IObservable<ContextBean> getOrCreateCompleteBeanEvent(ContextBean bean)
    {
        return bean.getInfo().getCustomData().getDataMap().getOrCreate(
                "swl3/ioc/completeBeanEvent",
                Observables::createObservable
        );
    }
}
