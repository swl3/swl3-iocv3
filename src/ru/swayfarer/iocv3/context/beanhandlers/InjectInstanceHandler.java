package ru.swayfarer.iocv3.context.beanhandlers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.iocv3.annotation.InjectInstance;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.thread.ThreadsUtils;

import java.lang.reflect.Field;

public class InjectInstanceHandler implements IFunction1NoR<ContextBean>
{
    @Internal
    public IFunction0<ExtendedMap<Class<?>, ExtendedList<CacheEntry>>> cachedInstanceFields = ThreadsUtils.singleton(ExtendedMap::new);

    @Internal
    public ReflectionsUtils reflectionsUtils = ReflectionsUtils.getInstance();

    @Internal
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();

    @Override
    public void applyNoRUnsafe(ContextBean bean) throws Throwable
    {
        bean.setValueFun(bean.getValueFun().andApply((o) -> {
            if (o == null)
                return o;

            var classOfValue = o.getClass();

            var instanceFields = cachedInstanceFields.apply().getOrCreate(classOfValue, () -> {
                var filters = reflectionsUtils.fields().filters();
                return reflectionsUtils.fields().stream(classOfValue)
                        .filter(filters.annotation().assignable(InjectInstance.class))
                        .not().filter(filters.mod().finals())

                        .throwIfNotPass((field) -> new IllegalStateException("Field " + field + " can't accept bean instance type " + classOfValue))
                        .filter(filters.type().canAssign(classOfValue))
                        .map((field) -> {
                            var entry = new CacheEntry();
                            entry.setField(field);

                            var injectIfFieldValueNonNull = reflectionsUtils.annotations().findProperty()
                                    .name("ifNonNull")
                                    .type(boolean.class)
                                    .in(field)
                                    .orElse(false)
                            ;

                            entry.setIfNonNull(injectIfFieldValueNonNull);

                            return entry;
                        })
                        .toExList()
                ;
            });

            for (var instanceField : instanceFields)
            {
                var field = instanceField.getField();
                var oldValue = reflectionsUtils.fields().getValue(field, o);

                if (oldValue == null || instanceField.isIfNonNull())
                {
                    reflectionsUtils.fields().setValue(field, o, o);
                }
            }

            return o;
        }));
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CacheEntry {
        public Field field;
        public boolean ifNonNull;
    }
}
