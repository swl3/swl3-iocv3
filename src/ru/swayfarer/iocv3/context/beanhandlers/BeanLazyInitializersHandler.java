package ru.swayfarer.iocv3.context.beanhandlers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.var;
import ru.swayfarer.iocv3.annotation.BeanLazyInitializer;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.componentscan.ComponentScan;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.asmv2.asm.Opcodes;
import ru.swayfarer.swl3.asmv2.proxy.ReflectionProxyHelper;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

public class BeanLazyInitializersHandler implements IFunction1NoR<ContextBean>
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public AsmUtilsV2 asmUtilsV2;

    @Internal
    @NonNull
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();

    @Internal
    @NonNull
    public ILogger logger = LogFactory.getLogger();

    public BeanLazyInitializersHandler(@NonNull ReflectionsUtils reflectionsUtils, @NonNull AsmUtilsV2 asmUtilsV2)
    {
        this.reflectionsUtils = reflectionsUtils;
        this.asmUtilsV2 = asmUtilsV2;
    }

    @Override
    public void applyNoRUnsafe(ContextBean bean) throws Throwable
    {
        if (ComponentScan.isBeanFromComponentScan(bean))
        {
            var initializationEntries = new ExtendedList<InitializationMethodEntry>();

            var mf = reflectionsUtils.methods().filters();
            var beanClass = ComponentScan.getBeanCreationClass(bean);
            reflectionsUtils.methods().stream(beanClass)
                    .filter(mf.annotation().assignable(BeanLazyInitializer.class))

                    .doIfNotPass((method) -> logger.warn("Method", method, "marked as bean lazy initializer but has args. Skipping..."))
                    .filter(mf.params().count().equal(0))

                    .doIfNotPass((method) -> logger.warn("Method", method, "marked as bean lazy initializer, but its not a public. Skipping..."))
                    .filter(mf.mod().publics())

                    .doIfNotPass((method) -> logger.warn("Method", method, "marked as bean lazy initializer, but its static. Skipping..."))
                    .not().filter(mf.mod().statics())

                    .each((method) -> {
                        var exceptionsTags = reflectionsUtils.annotations().findProperty()
                                .name("exceptionsTags")
                                .type(String[].class)
                                .marker(BeanLazyInitializer.class)
                                .in(method)
                                .optional().orElseGet(() -> new String[0])
                        ;

                        var executionPriority = reflectionsUtils.annotations().findProperty()
                                .name("priority")
                                .type(int.class)
                                .marker(BeanLazyInitializer.class)
                                .in(method)
                                .optional().orElse(0)
                        ;

                        var initializationEntry = InitializationMethodEntry.builder()
                                .exceptionsTags(exceptionsTags)
                                .priority(executionPriority)
                                .method(method)
                                .build()
                        ;

                        initializationEntries.add(initializationEntry);
                    })
            ;

            if (!CollectionsSWL.isNullOrEmpty(initializationEntries))
            {
                initializationEntries.sortBy(Comparator.comparingInt(InitializationMethodEntry::getPriority));

                // TODO maybe its not thread safe?
                var isInitiedFieldName = "__$_swl3_iocv3_is_bean_lazy_inited" + UUID.randomUUID().toString().replace("-", "");
                var proxyHelper = new ReflectionProxyHelper(asmUtilsV2, reflectionsUtils);

                IFunction1<Object, Boolean> isObjectAlreadyInitedFun = (o) -> {
                    return Boolean.TRUE.equals(reflectionsUtils.fields().getValue(o, isInitiedFieldName));
                };

                IFunction1NoR<Object> markAsAlreadyInitedFun = (o) -> {
                    reflectionsUtils.fields().setValue(o, true, isInitiedFieldName);
                };

                var proxyClassRef = new AtomicReference<Class<?>>();

                var newProxyClass = proxyHelper.newProxyClass(
                    (invocationEvent) -> {
                        var instance = invocationEvent.getInstance();

                        var alreadyInited = isObjectAlreadyInitedFun.apply(instance);
                        if (!alreadyInited)
                        {
                            synchronized (instance)
                            {
                                alreadyInited = isObjectAlreadyInitedFun.apply(instance);
                                if (!alreadyInited)
                                {
                                    markAsAlreadyInitedFun.apply(instance);
                                    initializationEntries.exStream()
                                        .filter((m) -> !m.getMethod().toString().equals(invocationEvent.getOverrideMethod().toString()))
                                        .each((initMethodEntry) -> {
                                            exceptionsHandler.handle()
                                                .tag(initMethodEntry.getExceptionsTags())
                                                .message("Error while processing lazy initialization method", "method", "of", instance)
                                                .start(() ->
                                                {
                                                    initMethodEntry.getMethod().invoke(instance);
                                                })
                                            ;
                                        })
                                    ;
                                }
                            }
                        }

                        invocationEvent.invokeSuperOrDefault();
                    },
                    (cfg) -> {
                           cfg
                               .setHandlerFieldStatic(true)
                               .setParentType(beanClass)
                               .newField(
                                       Opcodes.ACC_PUBLIC | Opcodes.ACC_VOLATILE,
                                       isInitiedFieldName,
                                       boolean.class
                               )
                           ;
                    }
                );

                proxyClassRef.set(newProxyClass);
                ComponentScan.setBeanCreationClass(bean, newProxyClass);
            }
        }
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @SuperBuilder
    public static class InitializationMethodEntry {
        public Method method;
        public String[] exceptionsTags;
        public int priority;
    }
}
