package ru.swayfarer.iocv3.context.beanhandlers;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.context.Context;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Data
@Accessors(chain = true)
public class LazyByDefaultHandler implements IFunction1NoR<ContextBean> {

    @NonNull
    public Context context;
    
    @Override
    public void applyNoRUnsafe(ContextBean bean)
    {
        var beanInfo = bean.getInfo();
        
        if (beanInfo.getLazy() == null)
        {
            beanInfo.setLazy(context.getContextSettings().isBeansLazyByDefault());
        }
    }

}
