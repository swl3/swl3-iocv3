package ru.swayfarer.iocv3.context.beanhandlers;

import lombok.var;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.context.Context;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns;

import java.util.UUID;

@Data
@Accessors(chain = true)
public class BeanProcessorsHandler implements GeneratedFuns.IFunction1NoR<ContextBean> {

    @NonNull
    public Context context;

    @Override
    public void applyNoRUnsafe(ContextBean contextBean) throws Throwable {
        var processors = getBeanPreProcessors();

        for (var proc : processors)
        {
            proc.apply(contextBean);
        }
    }

    public ExtendedList<GeneratedFuns.IFunction1NoR<ContextBean>> getBeanPreProcessors()
    {
        return Settings.getSettings(context).getBeanPreProcessors();
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    public static class Settings {

        public static String settingsKey = UUID.randomUUID().toString();

        @NonNull
        public ExtendedList<GeneratedFuns.IFunction1NoR<ContextBean>> beanPreProcessors = CollectionsSWL.list();

        public static Settings getSettings(Context context)
        {
            return context.getCustomData().getOrCreate(settingsKey, Settings::new);
        }
    }
}
