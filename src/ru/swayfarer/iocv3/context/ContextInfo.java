package ru.swayfarer.iocv3.context;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ContextInfo {
    @NonNull
    public String name = "";
}
