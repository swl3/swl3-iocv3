package ru.swayfarer.iocv3.exception;

public class IocException extends RuntimeException
{
    public IocException()
    {

    }

    public IocException(String message)
    {
        super(message);
    }

    public IocException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public IocException(Throwable cause)
    {
        super(cause);
    }
}
