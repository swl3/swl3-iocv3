package ru.swayfarer.iocv3.exception;

@SuppressWarnings("serial")
public class BeanInitializationException extends IocException {

    public BeanInitializationException()
    {
        super();
    }

    public BeanInitializationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public BeanInitializationException(String message)
    {
        super(message);
    }

    public BeanInitializationException(Throwable cause)
    {
        super(cause);
    }

    public static BeanInitializationException wrap(Throwable e)
    {
        if (e instanceof BeanInitializationException)
            return (BeanInitializationException) e;

        return new BeanInitializationException(e);
    }
}
