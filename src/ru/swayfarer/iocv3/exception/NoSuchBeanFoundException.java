package ru.swayfarer.iocv3.exception;

@SuppressWarnings("serial")
public class NoSuchBeanFoundException extends IocException {

	public NoSuchBeanFoundException() {
		super();
	}

	public NoSuchBeanFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoSuchBeanFoundException(String message) {
		super(message);
	}

	public NoSuchBeanFoundException(Throwable cause) {
		super(cause);
	}
}
