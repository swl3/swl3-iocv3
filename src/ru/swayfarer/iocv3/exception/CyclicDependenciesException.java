package ru.swayfarer.iocv3.exception;

@SuppressWarnings("serial")
public class CyclicDependenciesException extends IocException {

    public CyclicDependenciesException()
    {
        super();
    }

    public CyclicDependenciesException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public CyclicDependenciesException(String message)
    {
        super(message);
    }

    public CyclicDependenciesException(Throwable cause)
    {
        super(cause);
    }

}
