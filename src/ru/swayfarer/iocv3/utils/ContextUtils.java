package ru.swayfarer.iocv3.utils;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.OnDestroy;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.context.Context;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class ContextUtils {
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @NonNull
    public IFunction0<ExceptionsHandler> exceptionsHandler;
    
    @NonNull
    public Context context;
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
    
    public ContextUtils destroy()
    {
        if (!context.isDestroyed())
        {
            var exceptionsHandler = getExceptionsHandler();
            
            for (var bean : context.getBeans())
            {
                for (var value : bean.getAllCreatedValues())
                {
                    var reflectionsUtils = getReflectionsUtils();
                    var methodFilters = reflectionsUtils.methods().filters();

                    var destroyHandlers = reflectionsUtils.methods().stream(value)
                        .filter(methodFilters.annotation().assignable(OnDestroy.class))
                        .not().filter(methodFilters.mod().abstracts())
                        .not().filter(methodFilters.mod().statics())
                        .filter(methodFilters.params().count().equal(0))
                        .toExList()
                    ;
                    
                    for (var handlerMethod : destroyHandlers)
                    {
                        exceptionsHandler.safe(() -> {
                            handlerMethod.invoke(value);
                        }, "Error while processing bean destroy handler method", handlerMethod, "in", value);
                    }
                }
            }
            
            context.isDestroyed.set(true);
            
            context.getBeans().clear();
        }
        
        return this;
    }

    public ContextUtils addSingleton(String name, Class<?> beanClassType, IFunction0<Object> valueFun)
    {
        return addSingleton(name, valueFun, (bean) -> {
            bean.getInfo().getAccociatedType().setClassType(beanClassType);
        });
    }

    public ContextUtils addSingleton(String name, IFunction0<Object> valueFun, IFunction1NoR<ContextBean> beanConfigurer)
    {
        var contextBean = new ContextBean(valueFun);
        contextBean.getInfo().name(name);
        beanConfigurer.apply(contextBean);
        context.addBean(contextBean);
        return this;
    }
    
    public ExceptionsHandler getExceptionsHandler()
    {
        return exceptionsHandler.apply();
    }
}
