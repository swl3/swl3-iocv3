package ru.swayfarer.iocv3.injection;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

@Data
@NoArgsConstructor @AllArgsConstructor
@Accessors(chain = true)
public class BeansInjectionEvent {
    
    @NonNull
    public IContextInjector contextInjector;
    
    @NonNull
    public Object instanceToInject;

    @NonNull
    public IFunction1<ContextBean, Object> beanValueFun = ContextBean::getValue;
}
