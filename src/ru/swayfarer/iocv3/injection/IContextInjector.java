package ru.swayfarer.iocv3.injection;

import lombok.var;
import lombok.NonNull;

public interface IContextInjector {
    public Object inject(@NonNull Object object);
}
