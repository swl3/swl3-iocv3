package ru.swayfarer.iocv3.injection;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.finder.BeanFinder;
import ru.swayfarer.iocv3.context.finder.ContextFinder;
import ru.swayfarer.iocv3.injection.reflection.FieldsInjectionRule;
import ru.swayfarer.iocv3.injection.reflection.rules.SetFieldRule;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class ContextInjector implements IContextInjector{

    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;

    @NonNull
    public IFunction0<BeanFinder> beanFinder;
    
    public IObservable<BeansInjectionEvent> eventInjection = Observables.createObservable()
            .exceptions()
                .throwOnFail()
    ;
    
    @NonNull
    public IFunction0<ContextFinder> contextFinder;
    
    public Object inject(@NonNull Object target) 
    {
        var event = new BeansInjectionEvent()
                .setContextInjector(this)
                .setInstanceToInject(target)
        ;
        
        eventInjection.next(event);
        
        return target;
    }

    public BeanFinder getBeanFinder()
    {
        return beanFinder.apply();
    }
    
    public ContextFinder getContextFinder()
    {
        return contextFinder.apply();
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
    
    public FieldsInjectionRule getFieldsInjectionsRule()
    {
        var ret = new FieldsInjectionRule(this::getReflectionsUtils);
        
        ret.eventInjection.subscribe().by(new SetFieldRule(this::getBeanFinder, this::getReflectionsUtils));
        
        return ret;
    }
    
    public ContextInjector registerDefaultRules()
    {
        var fields = getFieldsInjectionsRule();
        
        if (fields != null)
            eventInjection.subscribe().by(fields);
        
        return this;
    }
    
    {
        registerDefaultRules();
    }
}
