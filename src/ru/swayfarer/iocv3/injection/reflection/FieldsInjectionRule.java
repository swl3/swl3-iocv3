package ru.swayfarer.iocv3.injection.reflection;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.var;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.Injection;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.injection.BeansInjectionEvent;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.thread.ThreadsUtils;

@Data
@Accessors(chain = true)
public class FieldsInjectionRule implements IFunction1NoR<BeansInjectionEvent> {

    @NonNull
    public IObservable<FieldBeanInjectionEvent> eventInjection = Observables.createObservable()
            .exceptions()
                .throwOnFail()
    ;
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @NonNull
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    
    @NonNull
    public IFunction0<ExtendedMap<Class<?>, ExtendedList<Field>>> cachedFields = ThreadsUtils.threadLocal(ExtendedMap::new);

    @Override
    public void applyNoRUnsafe(BeansInjectionEvent event)
    {
        var instance = event.getInstanceToInject();
        var injector = event.getContextInjector();
        
        if (instance != null && injector != null)
        {
            var fieldsToInject = findOrCreateFields(instance);
            
            for (var field : fieldsToInject) 
            {
                var fieldEvent = new FieldBeanInjectionEvent()
                        .setField(field)
                        .setInjectionRule(this)
                        .setInstance(instance)
                        .setBeansInjectionEvent(event)
                ;
                
                eventInjection.next(fieldEvent);
            }
        }
    }
    
    public ReflectionsUtils getReflectionsUtils() 
    {
        return reflectionsUtils.apply();
    }
    
    public ExtendedList<Field> findOrCreateFields(@NonNull Object instance)
    {
        var reflectionsUtils = getReflectionsUtils();
        var fieldsFilters = reflectionsUtils.fields().filters();

        var classOfInstance = instance.getClass();

        var ret = cachedFields.apply().getOrCreate(classOfInstance,
            () -> reflectionsUtils.fields().stream(instance)
                .filter(fieldsFilters.annotation().assignable(Injection.class))
                .toExList()
        );
        
        return ret;
    }
}
