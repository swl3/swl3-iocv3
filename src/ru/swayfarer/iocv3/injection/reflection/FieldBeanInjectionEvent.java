package ru.swayfarer.iocv3.injection.reflection;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.injection.BeansInjectionEvent;
import ru.swayfarer.swl3.observable.event.AbstractCancelableEvent;

import java.lang.reflect.Field;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class FieldBeanInjectionEvent extends AbstractCancelableEvent {
    public Field field;
    public Object instance;
    public BeansInjectionEvent beansInjectionEvent;
    
    public FieldsInjectionRule injectionRule;
}
