package ru.swayfarer.iocv3.injection.reflection.rules;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.finder.BeanFinder;
import ru.swayfarer.iocv3.bean.finder.FieldEventTarget;
import ru.swayfarer.iocv3.context.beanhandlers.BeanHandlingTrace;
import ru.swayfarer.iocv3.context.finder.event.ContextByFieldEvent;
import ru.swayfarer.iocv3.injection.reflection.FieldBeanInjectionEvent;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.thread.ThreadsUtils;

import java.lang.reflect.Field;

@Getter
@Setter
@Accessors(chain = true)
public class SetFieldRule implements IFunction1NoR<FieldBeanInjectionEvent> {

    @NonNull
    public IFunction0<BeanFinder> beanFinder;
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;

    @Internal
    public IFunction0<ExtendedMap<CacheKey, ContextBean>> cachedFieldBeans = ThreadsUtils.threadLocal(ExtendedMap::new);

    public SetFieldRule(@NonNull IFunction0<BeanFinder> beanFinder, @NonNull IFunction0<ReflectionsUtils> reflectionsUtils)
    {
        this.beanFinder = beanFinder;
        this.reflectionsUtils = reflectionsUtils;
    }

    @Override
    public void applyNoRUnsafe(FieldBeanInjectionEvent event)
    {
        var instance = event.getInstance();
        var findContextEvent = new ContextByFieldEvent();
        var field = event.getField();
        var classOfInstance = instance.getClass();
        var cacheKey = new CacheKey(classOfInstance, field);

        var foundBean = cachedFieldBeans.apply().getOrCreate(cacheKey, () -> {
            findContextEvent
                    .setTarget(
                            new FieldEventTarget(
                                    instance.getClass(),
                                    event.getField()
                            )
                            .setInstanceWithField(event.getInstance())
                    )
            ;

            var beanFinder = getBeanFinder();
            return beanFinder.findBean(instance.getClass(), event.getField(), instance);
        });

        if (foundBean != null)
        {
            var beansInjectionEvent = event.getBeansInjectionEvent();

            Object beanValue = beansInjectionEvent.getBeanValueFun().apply(foundBean);

            event.getInjectionRule().getExceptionsHandler().safe(() -> {
                field.set(instance, beanValue);
                BeanHandlingTrace.trace("Injecting field", field.getName(), "of", classOfInstance.getSimpleName());
                event.setCanceled(true);
            }, "Error while setting field", field, "of instance", instance, "to", beanValue);
        }
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
    
    public BeanFinder getBeanFinder()
    {
        return beanFinder.apply();
    }

    @Data
    @AllArgsConstructor
    public static class CacheKey {
        public Class<?> classOfInstance;
        public Field fieldOfInstance;
    }
}
