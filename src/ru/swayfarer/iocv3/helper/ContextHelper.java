package ru.swayfarer.iocv3.helper;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.finder.BeanFinder;
import ru.swayfarer.iocv3.bean.helper.BeanFindingBuilder;
import ru.swayfarer.iocv3.componentscan.ComponentScan;
import ru.swayfarer.iocv3.context.finder.ContextFinder;
import ru.swayfarer.iocv3.context.finder.rules.ContextPatternRule;
import ru.swayfarer.iocv3.context.registry.ContextRegistry;
import ru.swayfarer.iocv3.exception.BeanInitializationException;
import ru.swayfarer.iocv3.exception.CyclicDependenciesException;
import ru.swayfarer.iocv3.exception.NoSuchBeanFoundException;
import ru.swayfarer.iocv3.injection.ContextInjector;
import ru.swayfarer.iocv3.module.IoCV3Module;
import ru.swayfarer.iocv3.module.ModulesByDefault;
import ru.swayfarer.iocv3.sources.ContextSourceScanner;
import ru.swayfarer.iocv3.starter.IoCStarterV3;
import ru.swayfarer.iocv3.starter.MainMethodsFinder;
import ru.swayfarer.swl3.asmv2.classscan.ClassScanner;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.reflection.ClassUtil;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.ExpressionsList;
import ru.swayfarer.swl3.thread.group.ThreadGroupLocal;

@Getter
@Setter
@Accessors(chain = true)
public class ContextHelper {

    public ExtendedList<IoCV3Module> modules = new ExtendedList<>();

    @NonNull
    public static ThreadGroupLocal<ContextHelper> contextHelper = new ThreadGroupLocal<>();
    
    @NonNull
    public ContextFinder contextFinder = new ContextFinder(this::getContextRegistry);
    
    @NonNull
    public ContextInjector contextInjector = new ContextInjector(
            this::getReflectionsUtils, 
            this::getBeanFinder, 
            this::getContextFinder
    );
    
    @NonNull
    public ContextSourceScanner scanner = new ContextSourceScanner(
            () -> this
    );
    
    @NonNull
    public MainMethodsFinder mainMethodsFinder = new MainMethodsFinder(this::getReflectionsUtils);
    
    @NonNull
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    
    @NonNull
    public ReflectionsUtils reflectionsUtils = new ReflectionsUtils();
    
    @NonNull
    public ContextRegistry contextRegistry = new ContextRegistry(this::getExceptionsHandler, this::getContextInjector, this::getReflectionsUtils);
    
    @NonNull
    public BeanFinder beanFinder = new BeanFinder(this::getReflectionsUtils, this::getContextFinder);
    
    public IFunction0<ComponentScan> classPathComponentScan;
    
    public static ContextHelper getContextContextHelper()
    {
        return contextHelper.getValue();
    }

    public BeanFindingBuilder findBean()
    {
        return new BeanFindingBuilder(this);
    }

    /**
     * Получить контекстный обработчик событий или создать его
     * @return Обработчик
     */
    public static ContextHelper getOrCreateContextHelper()
    {
        var ret = getContextContextHelper();
        
        if (ret == null)
        {
            synchronized (contextHelper)
            {
                ret = getContextContextHelper();
                
                if (ret == null)
                {
                    ret = new ContextHelper();
                    setContextHelper(ret);
                }
            }
        }
        
        return ret;
    }
    
    public static void configureExceptionsHandler()
    {
        configureExceptionsHandler(ExceptionsHandler.getOrCreateContextHandler());
    }
    
    public static void configureExceptionsHandler(@NonNull ExceptionsHandler exceptionsHandler)
    {
        configureExceptionsHandler(exceptionsHandler, -1);
    }
    
    public static void configureExceptionsHandler(@NonNull ExceptionsHandler exceptionsHandler, int priority)
    {
        exceptionsHandler.configure()
                .rule(priority)
                    .type(
                            NoSuchBeanFoundException.class, 
                            BeanInitializationException.class,
                            CyclicDependenciesException.class
                    )
                    .thanSkip()
                    .throwEx()
        ;
    }
    
    public static ContextHelper setContextHelper(@NonNull ContextHelper helper)
    {
        contextHelper.value(helper);
        return helper;
    }
    
    public static ContextHelper startApp(@NonNull String... args)
    {
        return startAt(ExceptionsUtils.callerClass(), args);
    }
    
    @SneakyThrows
    public static ContextHelper startAt(@NonNull String appClassName, @NonNull String... args)
    {
    	return startAt(ClassUtil.forName(appClassName), args);
    }
    
    public static ContextHelper startAt(@NonNull Class<?> appClass, @NonNull String... args)
    {
        var contextHelper = ContextHelper.getOrCreateContextHelper();
        contextHelper.getStarter().start(appClass, args);
        return contextHelper;
    }
    
    public <T> T injectTo(T obj)
    {
        if (obj != null)
        {
            getContextInjector().inject(obj);
        }
        
        return obj;
    }
    
    public static <T> T injectContext(T obj)
    {
    	return getOrCreateContextHelper().injectTo(obj);
    }
    
    public IoCStarterV3 getStarter()
    {
        var ret = new IoCStarterV3(this::getReflectionsUtils, () -> this, this::getExceptionsHandler, this::getMainMethodsFinder);
        return ret;
    }
    
    public ContextHelper useContextInSubPackages(String contextName)
    {
        var name = ExceptionsUtils.classAt(2).getPackage().getName();
        return useContextInPackages(contextName, name);
    }
    
    public ContextHelper useContextInPackages(String contextName, @NonNull String... packages)
    {
        var contextFinder = getBeanFinder().getContextFinder();
        
        contextFinder.findByClassFuns.add(new ContextPatternRule(
                this::getReflectionsUtils, 
                () -> getContextRegistry().findOrCreateContext(contextName)
            )
            .setExpressionsList(new ExpressionsList(CollectionsSWL.list(packages)))
        );
        
        return this;
    }
    
    public void scanSource(Object source)
    {
        scanner.scan(source);
    }

    public void addModule(IoCV3Module module)
    {
        module.init(this);
        module.apply(this);
    }
    
    public ComponentScan getClassPathComponentScan()
    {
        return classPathComponentScan.apply();
    }
    
    {
        classPathComponentScan = () -> new ComponentScan(this::getReflectionsUtils, () -> this, AsmUtilsV2.getInstance(), new ClassScanner());
        classPathComponentScan = classPathComponentScan.memorized();

        ModulesByDefault.addToContext(this);
    }
}
