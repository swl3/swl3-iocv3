package ru.swayfarer.iocv3.componentscan;

import ru.swayfarer.swl3.collections.stream.ExtendedStream;

public interface IComponentScanProvider
{
    public ExtendedStream<Class<?>> findConfigurations();
    public ExtendedStream<Class<?>> findClasspathBeans();
}
