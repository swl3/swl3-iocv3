package ru.swayfarer.iocv3.componentscan;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.iocv3.annotation.Bean;
import ru.swayfarer.iocv3.annotation.BeanPriority;
import ru.swayfarer.iocv3.annotation.CanFindByType;
import ru.swayfarer.iocv3.annotation.ContextConfiguration;
import ru.swayfarer.iocv3.annotation.LazyBean;
import ru.swayfarer.iocv3.bean.BeanType;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.tags.IBeanTags;
import ru.swayfarer.iocv3.bean.tags.IBeanTags.BeanExceptions;
import ru.swayfarer.iocv3.context.beanhandlers.StandardBeanHandlers;
import ru.swayfarer.iocv3.exception.BeanInitializationException;
import ru.swayfarer.iocv3.helper.ContextHelper;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.classscan.ClassScanner;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.thread.ThreadsUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.concurrent.atomic.AtomicReference;

@Getter
@Setter
@Accessors(chain = true)
public class ComponentScan {

    @Internal
    public boolean traceComponentScan = false;

    public ILogger logger = LogFactory.getLogger();

    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @NonNull
    public IFunction0<ContextHelper> contextHelper;
    
    @NonNull
    public ClassScanner classScan;

    @NonNull
    public AsmUtilsV2 asmUtilsV2;

    @NonNull
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();

    @NonNull
    public IFunction0<ExtendedMap<Class<?>, Constructor<?>>> noArgsConstructorsCache = ThreadsUtils.threadLocal(ExtendedMap::new);

    public ComponentScan(
            @NonNull IFunction0<ReflectionsUtils> reflectionsUtils,
            @NonNull IFunction0<ContextHelper> contextHelper,
            @NonNull AsmUtilsV2 asmUtilsV2,
            @NonNull ClassScanner classScan
    )
    {
        this.reflectionsUtils = reflectionsUtils;
        this.contextHelper = contextHelper;
        this.classScan = classScan;
        this.asmUtilsV2 = asmUtilsV2;
    }

    public void scan(@NonNull String packageName)
    {
        var contextHelper = getContextHelper();
        var beanFinder = contextHelper.getBeanFinder();
        var reflectionsUtils = getReflectionsUtils();

        var cf = asmUtilsV2.classes().filters();
        // Загрузка контекстов
        classScan.stream(packageName)
            .filter(cf.aliasAnnotations().contains(ContextConfiguration.class))
            .not().filter(cf.mod().abstracts())
            .each((classNode) -> {
                var classType = Type.getObjectType(classNode.name);

                exceptionsHandler.handle()
                    .message("Error while scanning context source", classType.getClassName())
                    .tag(BeanExceptions.iocv3)
                    .tag(BeanExceptions.tagContextSourceScan)
                    .tag(BeanExceptions.tagClassScan)
                    .data(BeanExceptions.keyScannedContextSourceClassName, classType.getClassName())
                    .start(() ->
                    {
                        if (traceComponentScan)
                            logger.dev("Scanning class", classType.getClassName(), "as context source...");

                        var sourceClass = loadClass(classNode);
                        var newInstance = getReflectionsUtils().constructors()
                            .newInstance(sourceClass).orHandle(
                                    exceptionsHandler,
                                    (cfg) -> cfg
                                            .message("Can't create context configuration instance", sourceClass)
                                            .and().wrap(BeanInitializationException::wrap)
                            )
                        ;

                        contextHelper.scanSource(
                                newInstance
                        );
                    })
                ;
            })
        ;
        
        classScan.stream(packageName)
            .filter(cf.aliasAnnotations().contains(Bean.class))

            .each((classNode) ->
            {
                var classType = Type.getObjectType(classNode.name);
                exceptionsHandler.handle()
                    .message("Error while scanning bean class", classType.getClassName())
                    .tag(BeanExceptions.iocv3)
                    .tag(BeanExceptions.tagBeanClassScan)
                    .tag(BeanExceptions.tagClassScan)
                    .data(BeanExceptions.keyScannedContextBeanClassName, classType.getClassName())
                    .start(() ->
                    {
                        var beanClass = loadClass(classNode);

                        if (traceComponentScan)
                            logger.dev("Scanning class", classType.getClassName(), "as bean...");

                        var beanType = reflectionsUtils.annotations().findProperty()
                                .marker(Bean.class)
                                .name("type")
                                .type(String.class)
                                .in(beanClass)
                                .get()
                                ;


                        BeanType beanAssociatedType = beanFinder.getTypeFinder().findBeanType(beanClass);
                        ExtendedList<String> beanNames = beanFinder.getNameFinder().findNames(beanClass);

                        AtomicReference<ContextBean> beanRef = new AtomicReference<>();

                        var bean = new ContextBean(() -> {
                            var creationClass = getBeanCreationClass(beanRef.get());

                            if (Modifier.isAbstract(creationClass.getModifiers()))
                                ExceptionsUtils.throwException(new IllegalStateException("Can't create bean instance from abstract class. Maybe some custom logic does not exists?"));

                            var noArgsConstructor = noArgsConstructorsCache.apply().getOrCreate(creationClass, () -> {
                                var filtering = reflectionsUtils.constructors().filtering();

                                return reflectionsUtils.constructors().stream(creationClass)
                                        .filter(filtering.params().count().equal(0))
                                        .findFirst()
                                        .orElseThrow(() -> new BeanInitializationException("Can't find no-args constructor in " + creationClass))
                                        ;
                            });

                            return reflectionsUtils.constructors().newInstance(noArgsConstructor)
                                    .orHandle(
                                            exceptionsHandler,
                                            (cfg) -> cfg
                                                    .message("Can't create bean instance from", creationClass)
                                                    .and().wrap(BeanInitializationException::wrap)
                                    )
                                    ;
                        }
                        );

                        setBeanCreationClass(bean, beanClass);
                        beanRef.set(bean);

                        var isLazy = reflectionsUtils.annotations().findProperty()
                                .name("value")
                                .type(boolean.class)
                                .marker(LazyBean.class)
                                .in(beanClass)
                                .get()
                                ;

                        var canFindByType = reflectionsUtils.annotations().findProperty()
                                .name("value")
                                .type(boolean.class)
                                .marker(CanFindByType.class)
                                .in(beanClass)
                                .optional()
                                .orElse(false)
                                ;

                        var foundTags = contextHelper.getBeanFinder()
                                .getTagsFinder()
                                .findTags(beanClass)
                                ;

                        var context = beanFinder.getContextFinder().findContext(beanClass);

                        var beanPriority = reflectionsUtils.annotations().findProperty()
                                .name("value")
                                .type(int.class)
                                .marker(BeanPriority.class)
                                .in(beanClass)
                                .orElse(context.getContext().getContextSettings().getDefaultBeanPriority())
                                ;

                        bean.getInfo()
                                .data(IBeanTags.FromComponentScan)
                                .data(IBeanTags.StandartBean)
                                .data(foundTags)
                                .setCanFindByType(canFindByType)
                                .setBeanType(beanType)
                                .setAccociatedType(beanAssociatedType)
                                .setNames(beanNames)
                                .setPriority(beanPriority)
                        ;

                        if (isLazy != null)
                        {
                            bean.getInfo().setLazy(isLazy);
                        }

                        if (context != null)
                        {
                            context.getContext().addBean(bean);
                        }
                    })
                ;
            })
        ;
    }

    public static void setBeanCreationClass(ContextBean bean, Class<?> cl)
    {
        bean.getInfo().getCustomData().add("beanCreationClass", cl);
    }

    public static boolean isBeanFromComponentScan(ContextBean bean)
    {
        return bean.getInfo().getCustomData().has(IBeanTags.FromComponentScan);
    }

    public static Class<?> getBeanCreationClass(ContextBean bean)
    {
        return bean.getInfo().getCustomData().get("beanCreationClass");
    }

    public Class<?> loadClass(ClassNode classInfo)
    {
        return getReflectionsUtils().types().findClass(
                Type.getObjectType(classInfo.name).getClassName()
        ).orElseThrow();
    }

    public static void addLazyBeanCreationClassLoading(ContextBean bean, IFunction0<Class<?>> fun)
    {
        StandardBeanHandlers.addOnceAction(bean, () -> {
            var newClass = fun.apply();

            if (newClass != null)
                setBeanCreationClass(bean, newClass);
        });
    }

    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
    
    public ContextHelper getContextHelper()
    {
        return contextHelper.apply();
    }
}
