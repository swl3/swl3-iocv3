package ru.swayfarer.iocv3.starter;

import lombok.var;
import java.util.ArrayList;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.IocV3Starter;
import ru.swayfarer.iocv3.helper.ContextHelper;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.exception.LibTags;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.ExpressionsList;
import ru.swayfarer.swl3.swconf2.config.Swconf2Config;

@Data
@Accessors(chain = true)
public class IoCStarterV3 {
    
    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;
    
    @NonNull
    public IFunction0<ContextHelper> contextHelper;
    
    @NonNull
    public IFunction0<ExceptionsHandler> exceptionsHandler;
    
    @NonNull
    public IFunction0<MainMethodsFinder> mainMethodsFinder;
    
    public IoCStarterV3 start(Class<?> startClass, String... args)
    {
        var reflectionsUtils = getReflectionsUtils();
        
        var scanPackages = reflectionsUtils.annotations().findProperty()
            .type(String[].class)
            .name("scanPackages")
            .marker(IocV3Starter.class)
            .in(startClass)
            .get()
        ;
        
        var excludePackages = reflectionsUtils.annotations().findProperty()
            .type(String[].class)
            .name("scanExcludes")
            .marker(IocV3Starter.class)
            .in(startClass)
            .get()
        ;
        
        var includePackages = reflectionsUtils.annotations().findProperty()
            .type(String[].class)
            .name("scanIncludes")
            .marker(IocV3Starter.class)
            .in(startClass)
            .get()
        ;
        
        if (!CollectionsSWL.isNullOrEmpty(scanPackages))
        {
            var mainMethodsFinder = getMainMethodsFinder();
            var contextHelper = getContextHelper();
            var componentScan = contextHelper.getClassPathComponentScan();
            
            var packages = CollectionsSWL.list(scanPackages);
            var includes = new ExpressionsList(includePackages);
            var excludes = new ExpressionsList(excludePackages);
            
            var filteredPackages = packages.exStream()
                .filter((pkg) -> includes.isEmpty() || includes.isMatches(pkg))
                .filter((pkg) -> excludes.isEmpty() || !excludes.isMatches(pkg))
                .toExList()
            ;

            for (var pkg : filteredPackages)
            {
            	if (pkg.equals("."))
            	{
            		pkg = startClass.getPackage().getName();
            	}
            	
                componentScan.scan(pkg);
            }
            
            var exceptionsHandler = getExceptionsHandler();
            
            initNotLazyBeans(contextHelper);
            
            var mainMethodsLists = mainMethodsFinder.findMainMethods(contextHelper);
            
            for (var mainMethodsList : mainMethodsLists)
            {
                var beanValue = mainMethodsList.getBeanValue();
                
                for (var mainMethods : mainMethodsList.getMethods())
                {
                	exceptionsHandler.handle()
                		.tag(LibTags.Exceptions.tagAppStartup)
                		.message("Error while invoking main method handler", mainMethods.getMethod(), "in", beanValue)
                		.start(() -> {
                			mainMethods.getMethod().invoke(beanValue, new Object[] {args} );
                		})
            		;
                }
            }
        }
        
        return this;
    }
    
    public void initNotLazyBeans(@NonNull ContextHelper contextHelper)
    {
        var registry = contextHelper.getContextRegistry();
        var allContexts = registry.getRegisteredContexts().values();
        
        for (var context : allContexts)
        {
            for (var bean : context.getContext().getBeans())
            {
                if (!bean.getInfo().isLazy())
                {
                    var classType = bean.getInfo().getAccociatedType().getClassType();
                    
                    if (Swconf2Config.class.isAssignableFrom(classType))
                    {
                        bean.getValue();
                    }
                }
            }
        }
        
        for (var context : allContexts)
        {
            for (var bean : new ArrayList<>(context.getContext().getBeans()))
            {
                if (!bean.getInfo().isLazy())
                {
                    bean.getValue();
                }
            }
        }
    }
    
    public MainMethodsFinder getMainMethodsFinder()
    {
        return mainMethodsFinder.apply();
    }
    
    public ExceptionsHandler getExceptionsHandler()
    {
        return exceptionsHandler.apply();
    }
    
    public ContextHelper getContextHelper()
    {
        return contextHelper.apply();
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
