package ru.swayfarer.iocv3.starter;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.OnApplicationStart;
import ru.swayfarer.iocv3.helper.ContextHelper;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Data
@Accessors(chain = true)
public class MainMethodsFinder {

    @NonNull
    public IFunction0<ReflectionsUtils> reflectionsUtils;

    @Internal
    @NonNull
    public ILogger logger = LogFactory.getLogger();
    
    public ExtendedList<MainMethodsInfo> findMainMethods(@NonNull ContextHelper contextHelper)
    {
        var ret = new ExtendedList<MainMethodsInfo>();
        var registry = contextHelper.getContextRegistry();
        var allContexts = registry.getRegisteredContexts().values();
        var reflectionsUtils = getReflectionsUtils();
        
        for (var context : allContexts)
        {
            for (var bean : context.getContext().getBeans())
            {
                if (!bean.getInfo().isLazy())
                {
                    var beanValue = bean.getValue();
                    var mainMethodsInfo = new MainMethodsInfo();
                    mainMethodsInfo.setBeanValue(beanValue);
                    mainMethodsInfo.setBean(bean);
                    ret.add(mainMethodsInfo);

                    var methodFilters = reflectionsUtils.methods().filters();

                    var beanMainMethods = reflectionsUtils.methods().stream(beanValue)
                        .filter(methodFilters.annotation().assignable(OnApplicationStart.class))

                        .doIfNotPass((method) -> logger.warn("Method", method, "marked as application start handler, but its args not compatible with String[] param! Skipping..."))
                        .filter(methodFilters.params().canAcceptTypes(String[].class))

                        .not().filter(methodFilters.mod().abstracts())
                        .not().filter(methodFilters.mod().statics())
                        .toExList()
                    ;
                    
                    for (var method : beanMainMethods)
                    {
                        var mainMethodInfo = new MainMethodsInfo.MainMethod();
                        var priority = reflectionsUtils.annotations().findProperty()
                                .type(int.class)
                                .name("priority")
                                .marker(OnApplicationStart.class)
                                .in(method)
                                .get()
                        ;
                        
                        if (priority != null)
                        {
                            mainMethodInfo.setMethod(method);
                            mainMethodInfo.setPriority(priority);
                            mainMethodsInfo.methods.add(mainMethodInfo);
                        }
                    }
                }
            }
        }
        
        return ret;
    }
    
    public ReflectionsUtils getReflectionsUtils()
    {
        return reflectionsUtils.apply();
    }
}
