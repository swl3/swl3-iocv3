package ru.swayfarer.iocv3.starter;

import lombok.var;
import java.lang.reflect.Method;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.swl3.collections.list.ExtendedList;

@Data
@Accessors(chain = true)
public class MainMethodsInfo {

    public ContextBean bean;
    public Object beanValue;
    public ExtendedList<MainMethod> methods = new ExtendedList<>();
    
    @Data
    @Accessors(chain = true)
    public static class MainMethod {
        public Method method;
        public int priority;
    }
}
