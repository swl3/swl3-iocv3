package ru.swayfarer.iocv3.module;

import lombok.var;
import ru.swayfarer.iocv3.bean.ContextBean;
import ru.swayfarer.iocv3.bean.finder.BeanFinder;
import ru.swayfarer.iocv3.bean.tags.IBeanTags;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;

public class LoggingModule extends IoCV3Module{

    @Override
    public void registerBeanFinders(BeanFinder beanFinder) {

        beanFinder.eventFind.subscribe().by((event) -> {
            var targetBeanInfo = event.getTargetBeanInfo();
            var beanClassType = targetBeanInfo.getAccociatedType().getClassType();

            if (ILogger.class.equals(beanClassType) && targetBeanInfo.getCustomData().has(IBeanTags.Logging))
            {
                var bean = new ContextBean(() -> LogFactory.getLogger(beanClassType));
                bean.getInfo()
                    .setCanFindByType(false)
                    .getAccociatedType()
                    .setClassType(ILogger.class)
                ;

                bean.getInfo().getCustomData()
                    .add(IBeanTags.GeneratedBean)
                    .add(IBeanTags.Logging)
                    .add(IBeanTags.Temp)
                ;

                event.setResult(bean);
            }
        });
    }
}
