package ru.swayfarer.iocv3.module;

import lombok.var;
import ru.swayfarer.iocv3.helper.ContextHelper;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

public class ModulesByDefault {
    public static ExtendedList<IFunction1<ContextHelper, IoCV3Module>> modules = new ExtendedList<>().synchronyzed();

    public static void addToContext(ContextHelper contextHelper)
    {
        modules.each((moduleFun) -> contextHelper.addModule(moduleFun.apply(contextHelper)));
    }

    public static void registerDefaultModules()
    {
        modules.add((contextHelper) -> new DefaultModule());
        modules.add((contextHelper -> new SwconfModule()));
        modules.add((contextHelper -> new LoggingModule()));
    }

    static {
        registerDefaultModules();
    }
}
