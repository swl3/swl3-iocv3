package ru.swayfarer.iocv3.module;

import lombok.var;
import ru.swayfarer.iocv3.bean.finder.BeanFinder;
import ru.swayfarer.iocv3.bean.finder.name.BeanNameFinder;
import ru.swayfarer.iocv3.bean.finder.type.BeanTypeFinder;
import ru.swayfarer.iocv3.bean.tags.finder.BeanTagsFinder;
import ru.swayfarer.iocv3.context.finder.ContextFinder;
import ru.swayfarer.iocv3.context.registry.ContextRegistry;
import ru.swayfarer.iocv3.helper.ContextHelper;
import ru.swayfarer.iocv3.sources.ContextSourceScanner;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

public abstract class IoCV3Module {

    public ContextHelper contextHelper;

    public void init(ContextHelper contextHelper)
    {
        this.contextHelper = contextHelper;
    }

    public void registerContextFinders(ContextFinder contextFinder) {}
    public void registerBeanFinders(BeanFinder beanFinder) {}
    public void registerBeanHandlers(ContextUtils context) {}
    public void registerConfigScanHandlers(ContextSourceScanner contextSourceScanner) {}
    public void registerBeanTagsFinders(BeanTagsFinder beanTagsFinder) {}
    public void registerBeanTypeFinders(BeanTypeFinder beanTagsFinder) {}
    public void registerBeanNamesFinders(BeanNameFinder beanNameFinder) {}

    public void apply(ContextHelper contextHelper)
    {
        if (!contextHelper.modules.contains(this))
        {
            contextHelper.getContextRegistry().eventContextCreation.subscribe().by(this::registerBeanHandlers);
            registerContextFinders(contextHelper.getContextFinder());
            registerBeanFinders(contextHelper.getBeanFinder());
            registerConfigScanHandlers(contextHelper.getScanner());
            registerBeanTagsFinders(contextHelper.getBeanFinder().getTagsFinder());
            registerBeanTypeFinders(contextHelper.getBeanFinder().getTypeFinder());
            registerBeanNamesFinders(contextHelper.getBeanFinder().getNameFinder());

            contextHelper.modules.add(this);
        }
    }

    public ContextRegistry getContextRegistry()
    {
        return contextHelper.getContextRegistry();
    }

    public ReflectionsUtils getReflectionsUtils()
    {
        return contextHelper.getReflectionsUtils();
    }

    public BeanFinder getBeanFinder()
    {
        return contextHelper.getBeanFinder();
    }

    public ExceptionsHandler getExceptionsHandler()
    {
        return contextHelper.getExceptionsHandler();
    }

    public ContextFinder getContextFinder()
    {
        return contextHelper.getContextFinder();
    }
}
