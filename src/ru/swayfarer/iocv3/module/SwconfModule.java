package ru.swayfarer.iocv3.module;

import lombok.var;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.context.beanhandlers.Swconf2BeanHandler;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.swconf2.config.properties.ConfigPropertiesHelper;

@Getter
@Setter
@Accessors(chain = true)
public class SwconfModule extends IoCV3Module{

    public IFunction0<RLUtils> rlUtils = RLUtils::getInstance;
    public ConfigPropertiesHelper propertiesHelper = new ConfigPropertiesHelper();

    @Override
    public void registerBeanHandlers(ContextUtils context) {
        context.getContext().eventAddBean.subscribe().by(new Swconf2BeanHandler(
                rlUtils,
                this::getReflectionsUtils,
                propertiesHelper,
                context.getContext()
        ));
    }
}
