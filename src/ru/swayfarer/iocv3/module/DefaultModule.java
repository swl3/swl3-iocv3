package ru.swayfarer.iocv3.module;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.iocv3.bean.finder.BeanFinder;
import ru.swayfarer.iocv3.bean.finder.name.BeanNameFinder;
import ru.swayfarer.iocv3.bean.finder.name.rule.AnnotationsBeanNameRule;
import ru.swayfarer.iocv3.bean.finder.name.rule.ClassBeanNameRule;
import ru.swayfarer.iocv3.bean.finder.name.rule.FieldBeanNameRule;
import ru.swayfarer.iocv3.bean.finder.name.rule.MethodBeanNameRule;
import ru.swayfarer.iocv3.bean.finder.name.rule.ParameterBeanNameRule;
import ru.swayfarer.iocv3.bean.finder.rule.CurrentContextHelperRule;
import ru.swayfarer.iocv3.bean.finder.type.BeanTypeFinder;
import ru.swayfarer.iocv3.bean.finder.type.rules.AnnotationsBeanTypeRule;
import ru.swayfarer.iocv3.bean.finder.type.rules.ClassBeanTypeRule;
import ru.swayfarer.iocv3.bean.finder.type.rules.FieldBeanTypeRule;
import ru.swayfarer.iocv3.bean.finder.type.rules.MethodBeanTypeRule;
import ru.swayfarer.iocv3.bean.finder.type.rules.ParamBeanTypeRule;
import ru.swayfarer.iocv3.bean.tags.finder.BeanTagsFinder;
import ru.swayfarer.iocv3.bean.tags.finder.rules.AnnotationsBeanTagsFindRule;
import ru.swayfarer.iocv3.bean.tags.finder.rules.ClassBeanTagsFindRule;
import ru.swayfarer.iocv3.bean.tags.finder.rules.FieldBeanTagsFindRule;
import ru.swayfarer.iocv3.bean.tags.finder.rules.MethodBeanTagsFindRule;
import ru.swayfarer.iocv3.bean.tags.finder.rules.ParamBeanTagsFindRule;
import ru.swayfarer.iocv3.context.Context;
import ru.swayfarer.iocv3.context.Context.Priority;
import ru.swayfarer.iocv3.context.beanhandlers.BeanInitializersHandler;
import ru.swayfarer.iocv3.context.beanhandlers.BeanLazyInitializersHandler;
import ru.swayfarer.iocv3.context.beanhandlers.BeanProcessorsHandler;
import ru.swayfarer.iocv3.context.beanhandlers.BeanValueProcessorsHandler;
import ru.swayfarer.iocv3.context.beanhandlers.CompoundBeanHandler;
import ru.swayfarer.iocv3.context.beanhandlers.InjectInstanceHandler;
import ru.swayfarer.iocv3.context.beanhandlers.LazyByDefaultHandler;
import ru.swayfarer.iocv3.context.beanhandlers.StandardBeanHandlers;
import ru.swayfarer.iocv3.context.finder.ContextFinder;
import ru.swayfarer.iocv3.context.finder.rules.FieldAnnotationsRule;
import ru.swayfarer.iocv3.context.finder.rules.FindContextByClassRule;
import ru.swayfarer.iocv3.context.finder.rules.FindContextByMethodRule;
import ru.swayfarer.iocv3.context.finder.rules.FindContextByParamRule;
import ru.swayfarer.iocv3.sources.BeanProcessorsRule;
import ru.swayfarer.iocv3.sources.BeanValuesProcessorsRule;
import ru.swayfarer.iocv3.sources.ContextSourceScanner;
import ru.swayfarer.iocv3.sources.UseInPackagesRule;
import ru.swayfarer.iocv3.sources.method.ContextSettingsScanRule;
import ru.swayfarer.iocv3.sources.method.SourceMethodsScanRule;
import ru.swayfarer.iocv3.utils.ContextUtils;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;

@Getter
@Setter
@Accessors(chain = true)
public class DefaultModule extends IoCV3Module{

    @Override
    public void registerBeanNamesFinders(BeanNameFinder beanNameFinder)
    {
        var annotationsRule = new AnnotationsBeanNameRule(this::getReflectionsUtils);

        beanNameFinder.findByClassFuns.add(new ClassBeanNameRule(() -> annotationsRule));
        beanNameFinder.findByFieldFuns.add(new FieldBeanNameRule(() -> annotationsRule));
        beanNameFinder.findByMethodFuns.add(new MethodBeanNameRule(() -> annotationsRule));
        beanNameFinder.findByParamFuns.add(new ParameterBeanNameRule(AsmUtilsV2.getInstance(), () -> annotationsRule));
    }

    @Override
    public void registerBeanTypeFinders(BeanTypeFinder beanTagsFinder)
    {
        var annotations = new AnnotationsBeanTypeRule(this::getReflectionsUtils);

        beanTagsFinder.findByClassFuns.add(new ClassBeanTypeRule(this::getReflectionsUtils, () -> annotations));
        beanTagsFinder.findByFieldsFuns.add(new FieldBeanTypeRule(() -> annotations, this::getReflectionsUtils));
        beanTagsFinder.findByMethodFuns.add(new MethodBeanTypeRule(() -> annotations, this::getReflectionsUtils));
        beanTagsFinder.findByParamFuns.add(new ParamBeanTypeRule(() -> annotations, this::getReflectionsUtils));
    }

    @Override
    public void registerBeanTagsFinders(BeanTagsFinder beanTagsFinder)
    {
        var annotationsRule = new AnnotationsBeanTagsFindRule(this::getReflectionsUtils);

        beanTagsFinder.eventFindByClass.subscribe().by(new ClassBeanTagsFindRule(() -> annotationsRule));
        beanTagsFinder.eventFindByField.subscribe().by(new FieldBeanTagsFindRule(() -> annotationsRule));
        beanTagsFinder.eventFindByMethod.subscribe().by(new MethodBeanTagsFindRule(() -> annotationsRule));
        beanTagsFinder.eventFindByParam.subscribe().by(new ParamBeanTagsFindRule(() -> annotationsRule));
    }

    @Override
    public void registerConfigScanHandlers(ContextSourceScanner contextSourceScanner) {
        var eventScan = contextSourceScanner.eventScan;
        eventScan.subscribe().by(new BeanProcessorsRule(this::getReflectionsUtils, this::getContextFinder, this::getExceptionsHandler), 0);
        eventScan.subscribe().by(new BeanValuesProcessorsRule(this::getReflectionsUtils, this::getContextFinder, this::getExceptionsHandler), 2);
        eventScan.subscribe().by(new SourceMethodsScanRule(this::getBeanFinder, getExceptionsHandler(), getReflectionsUtils()), 2);
        eventScan.subscribe().by(new UseInPackagesRule(this::getReflectionsUtils), 2);

        eventScan.subscribe().by(new ContextSettingsScanRule(getExceptionsHandler(), getReflectionsUtils()));
    }

    @Override
    public void registerContextFinders(ContextFinder contextFinder) {
        contextFinder.findByClassFuns.add(new FindContextByClassRule(this::getContextRegistry, this::getReflectionsUtils));
        contextFinder.findByFieldFuns.add(new FieldAnnotationsRule(this::getContextRegistry, this::getReflectionsUtils));
        contextFinder.findByMethodFuns.add(new FindContextByMethodRule(this::getContextRegistry, this::getReflectionsUtils));
        contextFinder.findByMethodParamFuns.add(new FindContextByParamRule(this::getContextRegistry, this::getReflectionsUtils));
    }

    @Override
    public void registerBeanHandlers(ContextUtils contextUtils) {

        var context = contextUtils.getContext();
        var contextRegistry = getContextRegistry();
        var eventAddBean = context.getEventAddBean();

        eventAddBean.subscribe().by(new BeanValueProcessorsHandler(context, BeanValueProcessorsHandler.Settings::getPostProcessors), Priority.beanProcessorsPriority - 1);

        eventAddBean.subscribe().by(new InjectInstanceHandler(), Priority.injectInstancePriority);
        eventAddBean.subscribe().by(StandardBeanHandlers.injectBeans(contextUtils, contextRegistry.getContextInjector()), Context.Priority.injectorsPriority);
        eventAddBean.subscribe().by(new BeanInitializersHandler(contextRegistry::getReflectionsUtils, contextRegistry::getExceptionsHandler), Context.Priority.initializersPriority);
        eventAddBean.subscribe().by(new LazyByDefaultHandler(context), Priority.lazyPriority);
        eventAddBean.subscribe().by(new BeanProcessorsHandler(context), Context.Priority.beanProcessorsPriority);
        eventAddBean.subscribe().by(new BeanValueProcessorsHandler(context, BeanValueProcessorsHandler.Settings::getPreProcessors), Context.Priority.preProcessValuesPriority);
        eventAddBean.subscribe().by(new BeanLazyInitializersHandler(getReflectionsUtils(), AsmUtilsV2.getInstance()), Context.Priority.postProcessValuesPriority);
        eventAddBean.subscribe().by(StandardBeanHandlers::singletonBean, Context.Priority.preTypesPriority);
        eventAddBean.subscribe().by(StandardBeanHandlers::threadLocalBean, Context.Priority.preTypesPriority);
        eventAddBean.subscribe().by(StandardBeanHandlers::singletonBean, Priority.postTypesPriority);
        eventAddBean.subscribe().by(StandardBeanHandlers::threadLocalBean, Priority.postTypesPriority);
        eventAddBean.subscribe().by(new CompoundBeanHandler(AsmUtilsV2.getInstance(), contextHelper, getReflectionsUtils()), Priority.compoundBeans);
//        eventAddBean.subscribe().by(new CompleteBeanValueHandler(), Priority.completeBeans);
    }

    @Override
    public void registerBeanFinders(BeanFinder beanFinder) {
        beanFinder.eventFind.subscribe().by(new CurrentContextHelperRule(() -> contextHelper), BeanFinder.Priority.currentContextRulePriority);
    }
}
