package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Marks method as application start handler <br>
 * Can used only in not lazy {@link LazyBean} beans <br>
 *
 */
@Retention(RUNTIME)
public @interface OnApplicationStart {

    /**
     * Priority of application start handler <br>
     * At context start handlers will sort by this value <br>
     */
    public int priority() default 0;
}
