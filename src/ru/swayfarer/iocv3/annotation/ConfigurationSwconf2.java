package ru.swayfarer.iocv3.annotation;

import lombok.var;
import ru.swayfarer.iocv3.annotation.helpers.TypedSingleton;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Works only with component scanned {@link ru.swayfarer.swl3.swconf2.config.Swconf2Config} beans <br>
 * Creates bean with swconf-configuration logic
 * and allows injections of config properties with {@link InjectionByCfg} <br>
 *
 */
@TypedSingleton
@Retention(RUNTIME)
public @interface ConfigurationSwconf2 {

    /** Prefix for {@link InjectionByCfg} annotations injecting */
    public String prefix() default "";

    /**
     * Auto-save configuration delay. <br>
     * If configuration fields has observables types or config
     * and its changed or config will marked dirty something else way than
     * configuration will be saved automatically. <br>
     * @see ru.swayfarer.swl3.swconf2.config.Swconf2Config.ConfigHelper#enableAutoSave(long)
     */
    public long autoSaveDelay() default -1;

    /**
     * Configuration file location in ResourceLink format.
     * @see ru.swayfarer.swl3.io.link.RLUtils#createLink(String)
     */
    public String location();
}
