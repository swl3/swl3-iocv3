package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Using in pair with {@link BeanTag} annotation. <br>
 */
@Retention(RUNTIME)
public @interface CollectTags {

    /** Filter for collected tags */
    public String[] collectFilter() default {};

    /**
     * Allows to bean tags finder collect tags from parent elements. <br>
     * For example: class-owner is parent source for method or method is parent for param <br>
     */
    public boolean collectParents() default false;

    /**
     * Allows to bean tags finder collect tags from super annotations. <br>
     */
    public boolean collectSuper() default false;
}
