package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Allows to select current context name <br>
 * May be used with anyone class-scan IoC logic that's allows context selecting <br>
 * For example:
 * <pre>
 * {@code
 *      @ContextName("myContext")
 *      @Singleton
 *      public class MyBeanClass {}
 * }
 * </pre>
 * will create singleton bean in context "myContext"
 */
@Retention(RUNTIME)
public @interface ContextName {

    /** Context name value */
    public String value();
}
