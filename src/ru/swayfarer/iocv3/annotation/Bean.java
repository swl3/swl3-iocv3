package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Marks annotated element as bean <br>
 * Beans can be found by component scan
 */
@Retention(RUNTIME)
public @interface Bean {

    /** Singleton bean type. Singleton has only a single instance */
    public String Singleton = "singleton";

    /** Prototype bean type. Prototype has a multiple instances. Each new injection produces new instance. */
    public String Prototype = "prototype";

    /** ThreadLocal bean type. Produces a new instance for each thread */
    public String ThreadLocal = "threadlocal";

    /** Generated bean type. Custom bean instance logic by any beans generator */
    public String Generated = "generated";

    /** Bean type. Bean type is a basic marker for bean instance producing logic*/
    public String type() default Singleton;

    /** Bean associated java-class type. A basic markers for injections*/
    public Class<?> associatedType() default Object.class;
}
