package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Sets injection required state. If required bean will not be founded the exception will be throw
 */
@Retention(RUNTIME)
public @interface RequiredBean {

    /** Bean required property value */
    public boolean required() default true;
}
