package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Marks method as context destroy handler <br>
 * Method must has no params <br>
 * <h3>Warning: context destroy is OPTIONAL feature. Destroy handler may stay not called at all time!</h3>
 */
@Retention(RUNTIME)
public @interface OnDestroy {

}
