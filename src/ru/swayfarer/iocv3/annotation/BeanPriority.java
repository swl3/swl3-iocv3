package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Bean priority. <br>
 * Use in injection candidates sorting if more than one candidate will be found
 */
@Retention(RUNTIME)
public @interface BeanPriority {

    /** Bean priority value*/
    public int value();
}
