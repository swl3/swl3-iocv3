package ru.swayfarer.iocv3.annotation;

import lombok.var;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Injects object from context <br>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Injection {}
