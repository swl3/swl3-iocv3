package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Bean name selector <br>
 * Bean names used for injections marking
 */
@Retention(RUNTIME)
public @interface BeanName {

    /** Bean name value*/
    public String value() default "";

    /** Required name. Used in injections. If true, only beans with this name will be injection candidates*/
    public boolean required() default false;
}
