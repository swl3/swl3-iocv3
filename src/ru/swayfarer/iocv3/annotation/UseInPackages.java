package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 *  Apply context from current configuration at all selected packages their subpackages <br>
 *  May be used in context configuration classes <br>
 *  @see ContextConfiguration
 *  @see UseContextInThisPackage
 */
@Retention(RUNTIME)
public @interface UseInPackages {

    /** Packages to context apply */
    public String[] value();
}
