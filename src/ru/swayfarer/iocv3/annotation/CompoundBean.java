package ru.swayfarer.iocv3.annotation;

import ru.swayfarer.iocv3.context.beanhandlers.CompoundBeanHandler;
import ru.swayfarer.swl3.reflection.annotations.AnnotationAlias;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@BeanPriority(0)
@BeanTag(CompoundBeanHandler.compoundBeanTag)
@Retention(RetentionPolicy.RUNTIME)
public @interface CompoundBean
{
    @AnnotationAlias(value = BeanPriority.class, name = "value")
    public int beanPriority() default 0;
}
