package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

@Retention(RUNTIME)
public @interface BeanValuesProcessor {

    public EnumBeanState state() default EnumBeanState.PreInit;
    
    public static enum EnumBeanState {
        PreInit,
        PostInit;
    }
    
}
