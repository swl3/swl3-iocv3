package ru.swayfarer.iocv3.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface BeanLazyInitializer
{
    public String[] exceptionsTags() default {};
    public int priority() default 0;
}
