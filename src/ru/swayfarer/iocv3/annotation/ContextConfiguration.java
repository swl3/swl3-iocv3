package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Marks this class as context configuration <br>
 * Context configurations may have beans definitions,
 * context settings, bean processors and any customizable logic <br>
 *
 * @see BeanProcessor
 * @see BeanValuesProcessor
 * @see ContextSettingsEditor
 * @see Bean
 */
@Retention(RUNTIME)
public @interface ContextConfiguration {

}
