package ru.swayfarer.iocv3.annotation;

import lombok.var;
import ru.swayfarer.iocv3.helper.ContextHelper;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Gives information for {@link ContextHelper#startApp(String...)} about context settings. <br>
 */
@Retention(RUNTIME)
public @interface IocV3Starter {

    /** Packages to scan at context creation and configuration */
    public String[] scanPackages() default ".";

    /** Excludes for {@link #scanPackages()}*/
    public String[] scanExcludes() default {};

    /** Force includes for {@link #scanPackages()} if {@link #scanExcludes()} used*/
    public String[] scanIncludes() default {};
}
