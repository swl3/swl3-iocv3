package ru.swayfarer.iocv3.annotation;

import lombok.var;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Marks elements as bean processor. <br>
 * Bean processors used for preprocess beans after instance creation. <br>
 * It may proxy beans, add custom logic or something else <br>
 * <h3> It's will process BEANS*, not a VALUES </h3>
 * * - {@link ru.swayfarer.iocv3.bean.ContextBean}
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface BeanProcessor {
}
