package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import ru.swayfarer.iocv3.bean.tags.IBeanTags;
import ru.swayfarer.iocv3.helper.ContextHelper;

/**
 * Injects current context into target <br>
 * Marked target must be of assignable to @{@link ContextHelper} type. <br>
 */
@BeanTag(IBeanTags.CurrentContext)
@Injection
@Retention(RUNTIME)
public @interface InjectCurrentContext {

}
