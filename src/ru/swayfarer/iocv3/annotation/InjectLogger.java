package ru.swayfarer.iocv3.annotation;

import lombok.var;
import ru.swayfarer.iocv3.bean.tags.IBeanTags;
import ru.swayfarer.swl3.api.logger.ILogger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Injects logger into target. <br>
 * Target field must be of assignable for {@link ILogger} type <br>
 */
@Injection
@BeanTag(IBeanTags.Logging)
@Retention(RetentionPolicy.RUNTIME)
public @interface InjectLogger {
}
