package ru.swayfarer.iocv3.annotation.helpers;

import ru.swayfarer.iocv3.annotation.Bean;
import ru.swayfarer.iocv3.annotation.CanFindByType;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * {@link Bean} of {@link Bean#Prototype} type that can be found ignore name
 * @see CanFindByType
 */
@CanFindByType
@Prototype
@Retention(RetentionPolicy.RUNTIME)
public @interface TypedPrototype
{
}
