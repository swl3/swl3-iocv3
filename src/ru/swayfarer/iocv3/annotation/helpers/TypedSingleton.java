package ru.swayfarer.iocv3.annotation.helpers;

import ru.swayfarer.iocv3.annotation.Bean;
import ru.swayfarer.iocv3.annotation.CanFindByType;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * {@link Bean} of {@link Bean#Singleton} type that can be found ignore name
 * @see CanFindByType
 */
@CanFindByType
@Singleton
@Retention(RetentionPolicy.RUNTIME)
public @interface TypedSingleton {

}
