package ru.swayfarer.iocv3.annotation.helpers;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import ru.swayfarer.iocv3.annotation.LazyBean;

/**
 * {@link LazyBean} with lazy set to false
 */
@LazyBean(false)
@Retention(RUNTIME)
public @interface ForceLoadBean {

}
