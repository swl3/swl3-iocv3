package ru.swayfarer.iocv3.annotation.helpers;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import ru.swayfarer.iocv3.annotation.Bean;

/**
 * {@link Bean} of {@link Bean#Prototype} type
 */
@Bean(type = Bean.Prototype)
@Retention(RUNTIME)
public @interface Prototype {}
