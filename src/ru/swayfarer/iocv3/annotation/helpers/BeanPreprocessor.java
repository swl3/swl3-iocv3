package ru.swayfarer.iocv3.annotation.helpers;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import ru.swayfarer.iocv3.annotation.BeanValuesProcessor;
import ru.swayfarer.iocv3.annotation.BeanValuesProcessor.EnumBeanState;

/**
 * {@link BeanValuesProcessor} with {@link EnumBeanState#PreInit} state
 */
@BeanValuesProcessor(state = EnumBeanState.PreInit)
@Retention(RUNTIME)
public @interface BeanPreprocessor {

}
