package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import ru.swayfarer.iocv3.bean.tags.IBeanTags;
import ru.swayfarer.swl3.observable.property.ObservableProperty;
import ru.swayfarer.swl3.reflection.annotations.AnnotationAlias;

/**
 * Injects to target value from configurations <br>
 * Target property can be assignable of required property type or
 * {@link ObservableProperty} with some genetic assignable type. <br>
 * If observable property used configuration will react to property change <br>
 *
 * @see ConfigurationSwconf2
 */
@Injection
@BeanTag(IBeanTags.FromProperty)
@BeanName
@Retention(RUNTIME)
public @interface InjectionByCfg {

    /** Configuration property name */
    @AnnotationAlias(BeanName.class)
    public String value() default "";
}
