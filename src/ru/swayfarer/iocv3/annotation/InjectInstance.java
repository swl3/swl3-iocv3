package ru.swayfarer.iocv3.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface InjectInstance
{
    public boolean ifNonNull() default true;
}
