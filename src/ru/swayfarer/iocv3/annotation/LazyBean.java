package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Configures bean's lazy option. <br>
 * Lazy beans loads only its injected to something target. <br>
 * Not lazy beans loads at context start <br>
 */
@Retention(RUNTIME)
public @interface LazyBean {

    /** Lazy option value */
    public boolean value();
}
