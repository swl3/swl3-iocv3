package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Mark bean tags value <br>
 * Bean tags used for marking beans for custom handlers and injections <br>
 */
@Retention(RUNTIME)
public @interface BeanTag {
    public String[] value() default {};
}
