package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import ru.swayfarer.iocv3.context.settings.ContextSettings;
import ru.swayfarer.swl3.string.StringUtils;

/**
 * Marks method as context settings editor <br>
 * Allows co configure context after beans creation <br>
 * <br>
 * May be used on method with single {@link ContextSettings} param in context settings class
 *
 * @see ContextConfiguration
 */
@Retention(RUNTIME)
public @interface ContextSettingsEditor {

    /**
     * Allows to configure more than one context <br>
     * By Default accepts context of context-settings class that owns settings method <br>
     * Accepts expressions ({@link StringUtils#isMatchesByExpression(String, Object)} that filter target contexts name
     */
    public String[] value() default ".";
}
