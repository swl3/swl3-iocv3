package ru.swayfarer.iocv3.annotation;

import lombok.var;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Marks bean as available to type injecting ignore names <br>
 * Beans injections is highly-customizable logic, but in basic situation
 * beans injects by name and type. This annotation calls no-name injections.
 */
@Retention(RUNTIME)
public @interface CanFindByType {
	public boolean value() default true;
}
