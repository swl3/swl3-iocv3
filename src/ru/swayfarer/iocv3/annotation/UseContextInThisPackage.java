package ru.swayfarer.iocv3.annotation;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 *  Apply context from current configuration at this package and subpackages <br>
 *  May be used in context configuration classes <br>
 *  @see ContextConfiguration
 *  @see UseInPackages
 */
@Retention(RUNTIME)
public @interface UseContextInThisPackage {

}
