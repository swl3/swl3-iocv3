package ru.swayfarer.iocv3.annotation;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Marks method as bean init handler <br>
 * At bean init all injections must be completed <br>
 * Method must has no params <br>
 */
@Retention(RUNTIME)
public @interface OnBeanInit
{
    public String[] exceptionsTags() default {};

    public int priority() default 0;
}
